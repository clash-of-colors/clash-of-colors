#ifndef _VPAD_H_
#define _VPAD_H_

#include "fps.h"

#include <SDL.h>

struct VPAD
{
  //! \pre
  //! - SNES -
  //!  
  //!    |        X 
  //!  --+--    Y   A
  //!    |        B  
  //!
  //! \endpre
  
  enum Button {

    ButtonLeft,
    ButtonRight,
    ButtonUp,
    ButtonDown,

    ButtonA,
    ButtonB,
    ButtonX,
    ButtonY,
    
    ButtonVolumeUp,
    ButtonVolumeDown,

    ButtonTriggerLeft,
    ButtonTriggerRight,
    ButtonStart,
    ButtonSelect,

    MaxButtons = 32
  };

  bool state[MaxButtons];
  bool lastState[MaxButtons];

  VPAD();

  void reset();
  void update();
  bool isPressed(const Button button) const;
  bool wasPressed(const Button button) const;
  bool justPressed(const Button button) const;
  bool justPressedAny() const;
  bool justReleased(const Button button) const;

  void setPressed(const int button, bool pressed);
};

class VPADAdapter
{
public:
  virtual ~VPADAdapter();
  virtual void onSDLEvent(SDL_Event* event) = 0;
};

VPADAdapter* sysCreateVPADAdapter(VPAD* vpad);

class VPADRepeater
{
public:
  VPADRepeater(const int delay = fps::seconds(0.2f));
  void update(VPAD* vpad);
  bool justPressed(const int button) const;

private:
  int delay_;
  int counts_[VPAD::MaxButtons];
};

#endif /* _VPAD_H_ */
