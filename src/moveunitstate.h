#ifndef _MOVEUNITSTATE_H_
#define _MOVEUNITSTATE_H_

#include "state.h"

class Field;
class Scene;
class Unit;

class MoveUnitState : public State
{
public:
  MoveUnitState(ActionQueue& actions, Field* field, Scene* scene, STM::StateManager* stm);
  ~MoveUnitState();
  
  bool onInput(VPAD* vpad) override;

  void update() override;
  void render() override;

  void setup(Unit* unit);

  ActionQueue& actions;

  Field* field;
  Scene* scene;
  
  Unit* unit;

private:
  VPADRepeater vpadRepeater_;
};

#endif /* _MOVEUNITSTATE_H_ */
