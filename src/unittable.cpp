#include "unit.h"
#include "unittable.h"

#include <cassert>

#include <string.h> // memset

UnitTable::UnitTable(const std::vector<Unit*>& units)
{
  memset(units_, 0, sizeof(units_));
  for( auto unit : units ) {
    setUnit(unit);
  }
}

void UnitTable::set(const int col, const int row, Unit* unit)
{
  assert(col >= 0 && col < Field::Cols && row >= 0 && row < Rows);
  units_[col + row * Field::Cols] = unit;
}

Unit* UnitTable::get(const int col, const int row) const
{
  assert(col >= 0 && col < Field::Cols && row >= 0 && row < Rows);
  return units_[col + row * Field::Cols];
}

void UnitTable::setUnit(Unit* unit)
{
  setUnit(unit, unit);
}

void UnitTable::resetUnit(Unit* unit)
{
  setUnit(unit, 0);
}

void UnitTable::setUnit(Unit* unit, Unit* value)
{
  for( int row = unit->fieldPos.y; row < unit->fieldPos.y + unit->height; ++row ) {
    for( int col = unit->fieldPos.x; col < unit->fieldPos.x + unit->width; ++col ) {
      
      set(col, row, value);
    }
  }
}
