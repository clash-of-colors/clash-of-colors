#ifndef _ACTION_H_
#define _ACTION_H_

#include <memory>
#include <queue>

struct Action
{
  virtual ~Action() {}
  virtual bool isDone() const = 0;
  virtual void update() = 0;
};

typedef std::unique_ptr<Action> ActionPtr;
typedef std::queue<ActionPtr> ActionQueue;

class Field;
void updateActionQueue(ActionQueue& actions, Field* field, Field* otherField);

#endif /* _ACTION_H_ */
