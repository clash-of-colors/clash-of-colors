#ifndef _LAYERS_H_
#define _LAYERS_H_

namespace layers
{
  constexpr int base() { return 0; }
  constexpr int uiBase() { return 10; }

  constexpr int above(const int layer) { return layer + 1; }
  constexpr int below(const int layer) { return layer - 1; }
} // ns layers

#endif /* _LAYERS_H_ */
