#include "vpad.h"

#include <cstring>

VPAD::VPAD()
{
  reset();
}

void VPAD::reset()
{
  memset(state, 0, sizeof(state));
  memset(lastState, 0, sizeof(lastState));
}

void VPAD::update()
{
  memcpy(lastState, state, sizeof(lastState));
}

bool VPAD::isPressed(const Button button) const
{
  return state[button];
}

bool VPAD::wasPressed(const Button button) const
{
  return lastState[button];
}

bool VPAD::justPressed(const Button button) const
{
  return isPressed(button) && !wasPressed(button);
}

bool VPAD::justPressedAny() const
{
  for( int i = 0; i < MaxButtons; ++i ) {

    if( justPressed( (Button) i ) ) {

      return true;
    }
  }

  return false;
}

bool VPAD::justReleased(const Button button) const
{
  return wasPressed(button) && !isPressed(button);
}

void VPAD::setPressed(const int button, bool pressed)
{
  state[ button ] = pressed;
}

VPADAdapter::~VPADAdapter()
{
}

VPADRepeater::VPADRepeater(const int delay)
  : delay_(delay)
{
  memset(counts_, 0, sizeof(counts_));
}

void VPADRepeater::update(VPAD* vpad)
{
  for( int i = 0; i < VPAD::MaxButtons; ++i ) {

    if( vpad->justPressed( (VPAD::Button) i ) ) {
      
      counts_[i] = 0;
    }
    else if( vpad->isPressed( (VPAD::Button) i ) ) {

      counts_[i]++;
    }
    else {

      counts_[i] = -1;
    }
  }
}

bool VPADRepeater::justPressed(const int button) const
{
  const int c = counts_[button];
  const int delay = (c <= delay_ * 2) ? (delay_ * 2) : delay_;
  return (c % delay) == 0;
}
