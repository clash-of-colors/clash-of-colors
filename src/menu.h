#ifndef _MENU_H_
#define _MENU_H_

#include "action.h"

#include <functional>
#include <string>
#include <vector>

class MenuBox;
class MenuState;
class Scene;
class TextSceneNode;

typedef std::function<void(MenuState*)> ButtonCallback;

class Button
{
public:
  Button(const std::string& text, ButtonCallback callback);

  std::string text_;
  ButtonCallback callback_;
  MenuBox* menuBox_;
  TextSceneNode* textNode_;
};

class Menu
{
public:
  void addButton(const Button& button);
  void addButton(const std::string& text);

  void setupScene(Scene* scene, ActionQueue& actions);
  void cleanupScene(Scene* scene);

  std::vector<Button> buttons_;
};

#endif /* _MENU_H_ */
