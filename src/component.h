#ifndef _COMPONENT_H_
#define _COMPONENT_H_

class SceneNode;

class Component
{
public:
  virtual ~Component() {}
  virtual void update(SceneNode* node) = 0;
};

#endif /* _COMPONENT_H_ */
