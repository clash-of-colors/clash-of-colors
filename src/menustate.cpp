#include "alphaosc.h"
#include "aniscript.h"
#include "layers.h"
#include "menubox.h"
#include "menustate.h"
#include "runscriptaction.h"
#include "scene.h"

#include <stm/statemanager.h>

MenuState::MenuState(STM::StateManager* stateManager, const Menu& menu, Scene* scene)
  : State(stateManager)
  , menu_(menu)
  , scene_(scene)
{
}

void MenuState::update()
{
  if( !actions_.empty() ) {

    actions_.front()->update();
    if(actions_.front()->isDone()) {

      actions_.pop();
    }
  }
}

void MenuState::onTransition(STM::State*)
{
  if( getTransitionState() == TransitionIn ) {

    menu_.setupScene(scene_, actions_);

    MenuBox* firstButtonBox = menu_.buttons_.front().menuBox_;

    cursorBox_ = new MenuBox( firstButtonBox->pos, 
			      firstButtonBox->w_,
			      firstButtonBox->h_,
			      SPRITE_MENU_8X8_WHITE_0 );

    cursorBox_->addComponent(new AlphaOsc(1.0f, 0.7f));
    cursorBox_->layer = layers::above(firstButtonBox->layer);
    cursorBox_->visible = false;

    cursorIndex_ = 0;
    
    scene_->addNode(cursorBox_);

    actions_.push(ActionPtr(new RunScriptAction( ani::resetAlphaOsc(cursorBox_) )));
    actions_.push(ActionPtr(new RunScriptAction( ani::setVisible(cursorBox_, true) )));

    setTransitionState(TransitionInDone);
  }
  else {

    menu_.cleanupScene(scene_);
    cursorBox_->active = false;

    setTransitionState(TransitionOutDone);
  }
}

bool MenuState::onInput(VPAD* vpad)
{
  vpadRepeater_.update(vpad);

  if( actions_.empty() ) {

    if( vpadRepeater_.justPressed(VPAD::ButtonUp) ) {
      if( cursorIndex_ > 0 ) {

	cursorIndex_--;
	updateCursorPos();
      }
    }
    if( vpadRepeater_.justPressed(VPAD::ButtonDown) ) {
      
      if( cursorIndex_ + 1 < (int) menu_.buttons_.size() ) {
	
	cursorIndex_++;
	updateCursorPos();
      }
    }

    if( vpad->justPressed(VPAD::ButtonY) ) {
      menu_.buttons_[cursorIndex_].callback_(this);
    }

    if( vpad->justPressed(VPAD::ButtonStart) ||
	vpad->justPressed(VPAD::ButtonB) ) {
      // TODO: Only if allowed...
      getStateManager()->popState();
    }
  }

  return true;
}

void MenuState::updateCursorPos()
{
  MenuBox* buttonBox = menu_.buttons_[cursorIndex_].menuBox_;
  cursorBox_->pos = buttonBox->pos;
}
