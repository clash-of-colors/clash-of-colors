#include "position.h"

Position::Position()
  : x(0)
  , y(0)
{
}

Position::Position(float x, float y)
  : x(x)
  , y(y)
{
}

Position::Position(const Position& that)
  : x(that.x)
  , y(that.y)
{
}

Position& Position::operator=(const Position& that)
{
  x = that.x;
  y = that.y;
  
  return *this;
}

bool Position::operator==( const Position& that ) const
{
  return x == that.x && y == that.y;
}
