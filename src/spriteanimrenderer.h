#ifndef _SPRITEANIMRENDERER_H_
#define _SPRITEANIMRENDERER_H_

#include "scenenoderenderer.h"
#include "spriteanim.h"

class SpriteAnimRenderer : public SceneNodeRenderer
{
public:
  SpriteAnimRenderer();

  void setAnim( const spani::Anim* anim );

  void render(SceneNode* node) override;

  const spani::Runner& getRunner() const;
  
private:
  spani::Runner animRunner_;
};

#endif /* _SPRITEANIMRENDERER_H_ */
