#include "fps.h"

#include <algorithm>
#include <cmath>

namespace fps
{
  int seconds(const float s)
  {
    return std::max( 1, static_cast<int>(s * static_cast<float>(FPS)) );
  }

  int distanceVelocity(const float distance, const float velocity)
  {
    return seconds( std::abs(distance) / std::abs(velocity) );
  }

} // ns fps
