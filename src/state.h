#ifndef _STATE_H_
#define _STATE_H_

#include "vpad.h"

#include <stm/state.h>

class State : public STM::State
{
public:
  State(STM::StateManager* stm);

  void onTransition(STM::State* previousState) override;

  virtual bool onInput(VPAD* vpad);
};

#endif /* _STATE_H_ */
