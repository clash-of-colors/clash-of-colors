#include "spritenode.h"

#include <tmepp/video.h>

#include <cmath>

SpriteNode::SpriteNode(const int spriteID)
  : spriteID_(spriteID)
{
}

void SpriteNode::render()
{
  Tme::Video::blitSprite( spriteID_, 
			  roundf(pos.x), 
			  roundf(pos.y), 
			  static_cast<unsigned char>(roundf(alpha * 255.0f)) );
}
