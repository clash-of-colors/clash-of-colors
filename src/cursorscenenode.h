#ifndef _CURSORSCENENODE_H_
#define _CURSORSCENENODE_H_

#include "scenenode.h"

class Unit;

class CursorSceneNode : public SceneNode
{
public:
  CursorSceneNode();
  void update();
  void render();
  
  void setUnit(Unit* unit);

  Position fieldPos;
  Unit* unit;
};

#endif /* _CURSORSCENENODE_H_ */
