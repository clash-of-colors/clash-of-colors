#ifndef _PLAYERTURNSTATE_H_
#define _PLAYERTURNSTATE_H_

#include "action.h"
#include "turnstate.h"

class MoveCursorState;

class PlayerTurnState : public TurnState
{
public:
  PlayerTurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm);
  ~PlayerTurnState();
  
  void update();
  void render();
  void onTransition(STM::State* prevState);

private:
  ActionQueue actions;
  MoveCursorState* moveCursorState;
};

#endif /* _PLAYERTURNSTATE_H_ */
