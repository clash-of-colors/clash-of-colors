#include "aniscript.h"
#include "fps.h"
#include "globals.h"
#include "layers.h"
#include "menu.h"
#include "menubox.h"
#include "runscriptaction.h"
#include "scene.h"
#include "textscenenode.h"

#include <iostream>


Button::Button(const std::string& text, ButtonCallback callback)
  : text_(text)
  , callback_(callback)
  , menuBox_(0)
  , textNode_(0)
{
}

static void dummyButtonCallback(MenuState*)
{
  std::cerr << "dummyButtonCallback called..." << std::endl;
}

void Menu::addButton(const Button& button)
{
  buttons_.push_back(button);
}

void Menu::addButton(const std::string& text)
{
  buttons_.push_back(Button(text, dummyButtonCallback));
}

void Menu::setupScene(Scene* scene, ActionQueue& actions)
{
  const int Spacing = 4;
  const int ButtonH = 24;

  const int FontW = 10;
  const int FontH = 10;

  const int Margin = (ButtonH - FontH) / 2;
  
  const int MenuH = buttons_.size() * ButtonH + (buttons_.size() - 1) * Spacing;
  int y = (ScreenH - MenuH) / 2;

  int maxSize = 0;
  for( auto& button : buttons_ ) {

    maxSize = std::max(maxSize, (int) button.text_.size());
  }
  
  const int MenuW = maxSize * FontW + 2 * Margin;
  const int x = (ScreenW - MenuW) / 2;

  ani::Script script;

  for( auto& button : buttons_ ) {

    MenuBox* box = button.menuBox_ = new MenuBox(Position(x, y), MenuW, ButtonH);
    box->alpha = 0;
    box->layer = layers::uiBase();
    scene->addNode(box);

    const int textW = button.text_.size() * FontW;
    const int textX = (ScreenW - textW) / 2;
    const int textY = y + Margin;

    TextSceneNode* text = button.textNode_ = new TextSceneNode(Position(textX, textY), button.text_);
    text->alpha = 0;
    text->layer = layers::above(layers::above(layers::uiBase()));
    scene->addNode(text);

    script &= (ani::Script(box) << ani::setAlpha(1.0f, fps::seconds(1.0f), ani::interpolateSmoothstep));
    script &= (ani::Script(text) << ani::wait(fps::seconds(0.5f)) << ani::setAlpha(1.0f, fps::seconds(1.0f), ani::interpolateSmoothstep));

    script << ani::wait(fps::seconds(0.2f));

    y += ButtonH + Spacing;
  }

  actions.push(ActionPtr(new RunScriptAction(script)));
}

void Menu::cleanupScene(Scene* /*scene*/)
{
  for( auto& button : buttons_ ) {

    button.menuBox_->active = false;
    button.textNode_->active = false;
  }
}
