#include "alphaosc.h"
#include "scenenode.h"
#include "fps.h"

#include <cmath>

constexpr float pif()
{
#if defined(M_PI)
  return static_cast<float>(M_PI);
#else
  return std::atan(1.0f) * 4.0f;
#endif
}

AlphaOsc::AlphaOsc(const float frequency, const float amplitude)
  : arg_( pif() )
  , dArg_( frequency * (2.0f * pif() / static_cast<float>(FPS)) )
  , amplitude_(amplitude * 0.5f)
{
}

void AlphaOsc::update(SceneNode* node)
{
  node->alpha = (1.0f + std::cos(arg_)) * amplitude_;
  arg_ += dArg_;
}

void AlphaOsc::reset()
{
  arg_ = pif();
}
