#include "fps.h"
#include "scenenode.h"
#include "spriteanimrenderer.h"

#include <tmepp/video.h>

#include <cmath>

SpriteAnimRenderer::SpriteAnimRenderer()
  : animRunner_(FPS)
{
}

void SpriteAnimRenderer::setAnim( const spani::Anim* anim )
{
  animRunner_.setAnim(anim);
}

void SpriteAnimRenderer::render(SceneNode* node)
{
  const Sprite& sprite = animRunner_.getSprite();

  Tme::Video::blitSprite( sprite.spriteID, 
			  roundf(node->pos.x + sprite.offsetX), 
			  roundf(node->pos.y + sprite.offsetY), 
			  static_cast<unsigned char>(roundf(node->alpha * 255.0f)) );

  animRunner_.update();
}

const spani::Runner& SpriteAnimRenderer::getRunner() const
{
  return animRunner_;
}
