#include "aniscript.h"
#include "blinker.h"
#include "fps.h"
#include "gamestate.h"
#include "globals.h"
#include "layers.h"
#include "menu.h"
#include "menustate.h"
#include "runscriptaction.h"
#include "scenenode.h"
#include "spritenode.h"
#include "sprites.h"
#include "textscenenode.h"
#include "tilemap.h"
#include "tilemaprenderer.h"
#include "titlestate.h"

#include <stm/statemanager.h>

#include <cmath>

static TileMap* makeBGMap()
{
  const int numCycles = 1 + ceilf(static_cast<float>(ScreenW) / (4.0f * 16.0f));
  const int numCols = numCycles * 4;
  const int numRows = ceilf(static_cast<float>(ScreenH) / 32.0f);

  TileMap* map = new TileMap(numCols, numRows, 16, 32);
  
  TileType const tiles[4] = { SPRITE_BG_STRIPE_0, 
			      SPRITE_BG_STRIPE_1, 
			      SPRITE_BG_STRIPE_2, 
			      SPRITE_BG_STRIPE_3 };

  int i = 0;
  int offset = 0;
  for( int row = 0; row < numRows; ++row ) {

    for( int col = 0; col < numCols; ++col ) {

      map->set(i++, tiles[ (col + offset) % 4 ]);
    }

    offset = (offset + 1) % 4;
  }

  return map;
}

struct BG : public SceneNode
{
  TileMap* map;
  TileMapRenderer* renderer;

  BG()
    : map(makeBGMap())
    , renderer(new TileMapRenderer(map))
  {
    pos.x = -16 * 4;
  }

  ~BG()
  {
    delete map;
  }

  void render() override
  {
    renderer->render(this);

    // 30 px / s
    pos.x = pos.x + (30.0f / static_cast<float>(FPS));
    if( roundf(pos.x) >= 0 )
      pos.x = -16 * 4;
  }
};

TitleState::TitleState(STM::StateManager* stateManager)
  : State(stateManager)
{
  setName("title");

  scene_.addNode(new BG);

  const int LogoW = 205;
  const int LogoH = 135;

  logo_ = new SpriteNode(SPRITE_LOGO);
  logo_->pos.x = (ScreenW - LogoW) / 2;
  logo_->pos.y = (ScreenH - LogoH) / 3;
  logo_->layer = layers::above(layers::base());
  scene_.addNode(logo_);

  const int TextW = 12 * 10;
  const int textX = (ScreenW - TextW) / 2;
  const int textY = ScreenH - 40;

  text_ = new TextSceneNode(Position(textX, textY), "PRESS BUTTON");
  text_->layer = layers::above(layers::base());
  Blinker* blinker = new Blinker(0.5f);
  blinker->stop();
  text_->addComponent(blinker);
  scene_.addNode(text_);
}

void TitleState::update()
{
  if( !actions_.empty() ) {

    actions_.front()->update();
    if( actions_.front()->isDone() ) {

      actions_.pop();
    }
  }
  
  scene_.update();
}

void TitleState::render()
{
  scene_.render();
}

void TitleState::onTransition(STM::State*)
{
  if( getTransitionState() == TransitionIn ) {

    logo_->alpha = 0;
    logo_->visible = true;

    text_->visible = false;

    Blinker* blinker = text_->getComponent<Blinker>();
    blinker->stop();

    ani::Script script(logo_);
    script << ani::wait(fps::seconds(2));
    script << ani::setAlpha(1.0f, fps::seconds(2), ani::interpolateSmoothstep);
    
    script << ani::Script(text_);
    script << ani::startBlink();
    
    actions_.push(ActionPtr(new RunScriptAction(script)));

    setTransitionState(TransitionInDone);
  }
  else {

    setTransitionState(TransitionOutDone);
  }
}

bool TitleState::onInput(VPAD* vpad)
{
  if( actions_.empty() && vpad->justPressedAny() ) {

    logo_->visible = false;
    text_->getComponent<Blinker>()->stop();
    text_->visible = false;
    
    Menu menu;
    menu.addButton(Button("PLAYER VS. CPU", [](MenuState* state) {
	  state->getStateManager()->popState();
	  state->getStateManager()->swapTopState( new GameState(state->getStateManager(), false), true );
	}));

    menu.addButton(Button("CPU VS. CPU", [](MenuState* state) {
	  state->getStateManager()->popState();
	  state->getStateManager()->swapTopState( new GameState(state->getStateManager(), true), true );
	}));
  
    menu.addButton(Button("QUIT", [](MenuState* state) {
	  state->getStateManager()->popAll();
	}));
    
    getStateManager()->pushState(new MenuState(getStateManager(), menu, &scene_), true);
  }

  return true;
}
