#include "cursorscenenode.h"
#include "globals.h"
#include "layers.h"
#include "sprites.h"
#include "unit.h"

#include <tmepp/video.h>

#include <cassert>

CursorSceneNode::CursorSceneNode()
  : unit(0)
{
  layer = 1;
}

void CursorSceneNode::update()
{
}

void CursorSceneNode::render()
{
  int spriteId = -1;

  if( !unit ) {
    
    spriteId = SPRITE_CURSOR_CELL;
  }
  else {

    switch(unit->type) {

    case Unit::Single:
    case Unit::Wall:
      spriteId = SPRITE_CURSOR_SINGLE;
      break;

    case Unit::Quad:
    case Unit::QuadCharging:
      spriteId = SPRITE_CURSOR_QUAD;
      break;
      
    case Unit::TripleCharging:
      spriteId = SPRITE_CURSOR_TRIPLE;
      break;

    default:
      assert(false);
    }
  }

  assert(spriteId != -1);

  Tme::Video::blitSprite(spriteId, pos.x, pos.y);
}

void CursorSceneNode::setUnit(Unit* unit)
{
  if( this->unit ) {

    this->unit->layer = layers::base();
  }
  
  this->unit = unit;

  if( this->unit ) {

    this->unit->layer = layers::above(this->layer);
  }
}
