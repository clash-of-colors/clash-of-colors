#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_

#include "field.h"
#include "scene.h"
#include "state.h"

class TurnState;

class GameState : public State
{
public:
  GameState(STM::StateManager* stm, const bool cpuMatch);
  ~GameState();

  void onTransition( STM::State* prevState );

  void update();
  void render();

  bool onInput(VPAD* vpad) override;

  void pushMoveUnitState();

  Scene scene;
  Field fieldCpu;
  Field fieldPlayer;

  TurnState* cpuTurnState;
  TurnState* playerTurnState;
};

#endif /* _GAMESTATE_H_ */
