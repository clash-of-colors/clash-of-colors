#ifndef _MOVECURSORSTATE_H_
#define _MOVECURSORSTATE_H_

#include "position.h"
#include "state.h"

class CursorSceneNode;
class Field;
class MoveUnitState;
class Scene;
class Unit;

class MoveCursorState : public State
{
public:
  MoveCursorState(ActionQueue& actions, Field* field, Scene* scene, STM::StateManager* stm);
  ~MoveCursorState();

  void update() override;
  bool onInput(VPAD* vpad) override;

  void setup(int col);

  void updateCursor();
  void destroyCursor();

  void onTransition(STM::State* prevState) override;

  ActionQueue& actions;

  Field* field;
  Scene* scene;

  MoveUnitState* moveUnitState;

  bool endTurn;

  CursorSceneNode* cursor;

private:
  VPADRepeater vpadRepeater_;
};

#endif /* _MOVECURSORSTATE_H_ */
