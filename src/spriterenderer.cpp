#include "scenenode.h"
#include "spriterenderer.h"

#include <tmepp/video.h>

#include <cmath>

void SpriteRenderer::setSprite( const Sprite& sprite )
{
  sprite_ = sprite;
}

void SpriteRenderer::render(SceneNode* node)
{
  Tme::Video::blitSprite( sprite_.spriteID, 
			  roundf(node->pos.x + sprite_.offsetX), 
			  roundf(node->pos.y + sprite_.offsetY), 
			  static_cast<unsigned char>(roundf(node->alpha * 255.0f)) );
}
