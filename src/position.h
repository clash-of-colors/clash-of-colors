#ifndef _POSITION_H_
#define _POSITION_H_

class Position
{
public:

  Position();
  Position(float x, float y);
  Position(const Position& that);
  Position& operator=(const Position& that);

  bool operator==( const Position& that ) const;

  float x;
  float y;
};

#endif /* _POSITION_H_ */
