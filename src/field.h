#ifndef _FIELD_H_
#define _FIELD_H_

#include "action.h"

#include <vector>

class Position;
class Scene;
class Unit;

class Field
{
public:
  static const int Rows = 6;
  static const int Cols = 12;
  static const int CellW = 16;
  static const int CellH = 14;
  static const int CellMarginX = 1;
  static const int CellMarginY = 0;

  Field(Scene* scene, const int y0, const float sy);
  ~Field();

  int getDropRow( const Unit* unitToDrop, const int col ) const;
  bool canDropUnit( const Unit* unit, const int col ) const;
  void dropUnit( Unit* unit, const int col );
  Unit* getUnitAt( const Position& pos ) const;
  Unit* takeUnitAt( const Position& pos );
  void getUnitsInCols(const int col, const int numCols, std::vector<Unit*>& result) const;
  Unit* takeUnit( Unit* unit );
  void setUnitPos(Unit* unit) const;
  Position getUnitPos(const Unit* unit) const;
  Position getUnitPos(const Unit* unit, const int col, const int row) const;
  Position getUnitFloatingPos(const Unit* unit, const int col) const;
  Position getUnitSpawnPos(const Unit* unit, const int col) const;

  void checkActions(ActionQueue& actions);
  void checkAttack(Field* otherField, ActionQueue& actions);
  void onStartOfTurn();

  const std::vector<Unit*>& getUnits() const;

  int getY0() const { return y0_; }

  Scene* scene;
  int health;

private:
  void mergeUnits( Unit* bottomUnit, 
		   const std::vector<Unit*>& unitsToMerge,
		   Unit* mergedUnit,
		   ActionQueue& actions );

  void makeWalls( const std::vector<Unit*>& wallUnits, ActionQueue& actions );

  const int y0_;
  const float sy_;
  std::vector<Unit*> units_;
};

#endif /* _FIELD_H_ */
