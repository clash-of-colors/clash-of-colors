#ifndef _PUFF_H_
#define _PUFF_H_

class Position;
class SceneNode;

SceneNode* makePuff(const Position& pos);

#endif /* _PUFF_H_ */
