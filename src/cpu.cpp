#include "aniscript.h"
#include "cpu.h"
#include "field.h"
#include "fps.h"
#include "globals.h"
#include "puff.h"
#include "runscriptaction.h"
#include "scene.h"
#include "unit.h"
#include "unittable.h"
#include "waitaction.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>

Cpu::Action::Action()
  : type(Null)
  , dstCol(0)
{
}
    
Cpu::Action::Action(const Action& that)
  : type(that.type)
  , srcPos(that.srcPos)
  , dstCol(that.dstCol)
{
}

void Cpu::makeMoveActions( ActionQueue& actions, Field* field, Field* /*otherField*/, Scene* scene )
{
  // Make a list of possible moves that lead to a combo in N moves
  // 
  // If not combos can be achieved, call reinforcements
  // 
  // Make a list of columns worth attacking
  // * Light resistance -> damage player
  // * Un-charged power unit -> prevent power unit deployment
  // * Damage charging unit -> soften the blow
  // * Avoid walls 
  // 
  // Make a list of columns worth defending
  // * Light resistance -> prevent damage
  // * Un-charged power unit -> prevent loss of unit
  // * Charging units -> prevent loss of attack power
  // 
  // Sort possible moves by 
  // * Number of moves
  // * Target column
  // * Time to deploy
  // 
  // Implement moves
  //
  // Re-evaluate after enemy attack (units might be gone)


  // Moves:
  // * Drop unit onto other unit to build vertical combo
  //   Single: Need to get two more singles (Cost 2)
  //   Double: Need to get one more singles (Cost 1)
  // * Drop unit next to other unit to build wall
  // * Remove unit to build combo
  // * Move unit to build combo
  // * Remove unit to make room for combo

  MoveList moves;
  analyzeTriples(field, moves);

  if( moves.empty() ) {

    // Nothing to do, call reinforcements

    Move move;
    move.push_back(Action());
    move.back().type = Action::CallReinforcements;

    moves.push_back(move);
  }

  // Determine cheapest action
  Move cheapest;
  for( auto const& move : moves ) {
    
    if( cheapest.empty() || cheapest.size() > move.size() )
      cheapest = move;
  }
  
  const Action& cpuAction = cheapest.front();
  switch( cpuAction.type ) {

  case Action::Null:
    break;
    
  case Action::MoveUnit:
    {
      Unit* unit = field->takeUnitAt(cpuAction.srcPos);
      assert(unit);

      const Position pos0 = unit->pos;
      const Position pos1 = field->getUnitFloatingPos(unit, unit->fieldPos.x);
      const Position pos2 = field->getUnitFloatingPos(unit, cpuAction.dstCol);

      field->dropUnit( unit, cpuAction.dstCol );
      const Position pos3 = field->getUnitPos(unit);

      ani::Script script(unit);
      script << ani::moveTo( pos1.x,
			     pos1.y,
			     fps::distanceVelocity( pos1.y - pos0.y, MoveVelocity ),
			     ani::interpolateSmoothstep )
             << ani::moveTo( pos2.x,
			     pos2.y,
			     fps::distanceVelocity( pos2.x - pos1.x, MoveVelocity ),
			     ani::interpolateSmoothstep )
             << ani::moveTo( pos3.x,
			     pos3.y,
			     fps::distanceVelocity( pos3.y - pos2.y, MoveVelocity ),
			     ani::interpolateSmoothstep );
      
      std::cerr << "Action: Move unit (CPU)" << std::endl;
      actions.push(ActionPtr(new RunScriptAction(script)));
    }
    break;

  case Action::RemoveUnit:
    {
      Unit* unit = field->takeUnitAt(cpuAction.srcPos);
      assert(unit);

      unit->active = false;

      SceneNode* puff = makePuff(unit->getCenterPos());
      scene->addNode(puff);

      ani::Script script(puff);
      script << ani::waitForSpriteAnim()
	     << ani::kill();

      std::cerr << "Action: Remove unit (CPU)" << std::endl;
      actions.push(ActionPtr(new RunScriptAction(script)));
    }
    break;

  case Action::CallReinforcements:
    {
      // TODO: Refactor this, code is almost identical to player implementation... 

      ani::Script script;
      
      for( int i = 0; i < 5; ++i ) {
	
	Unit::Type type = (Unit::Type) ((rand() % 10) ? Unit::Single : Unit::Quad);
	Unit::Color color = (Unit::Color) (rand() % 3);
	
	Unit* unit = new Unit(type, color);
	
	int col = rand() % (Field::Cols + 1 - unit->width);
	
	int j = 0;
	for( ; j < Field::Cols; ++j, col = (col + 1) % Field::Cols ) {

	  if( field->canDropUnit(unit, col) ) {
	  
	    field->dropUnit(unit, col);

	    const Position pos0 = field->getUnitSpawnPos(unit, col);
	    const Position pos1 = field->getUnitPos(unit);
	    
	    unit->pos = pos0;
	    scene->addNode(unit);

	    script << ani::Script(unit) 
		   << ani::moveTo( pos1.x, pos1.y,
				   fps::distanceVelocity( pos1.y - pos0.y, MoveVelocity ),
				   ani::interpolateSmoothstep );
	    break;
	  }
	}
	if( j == Field::Cols ) {

	  std::cerr << "Could not drop unit..." << std::endl;
	  delete unit;
	}
      }
      
      std::cerr << "Action: Reinforcements (CPU)" << std::endl;
      actions.push(ActionPtr(new RunScriptAction(script)));
    }
    break;
  }
}

void Cpu::analyzeTriples(Field* field, std::vector<Move>& moves)
{
  UnitTable table(field->getUnits());

  // Loop through all single units 
  // and determine what it would take to build a triple combo
  // on top of them...

  for( int row = 0; row < Field::Rows - 2; ++row ) {
    for( int col = 0; col < Field::Cols; ++col ) {

      Unit* unit = table.get(col, row);
      if( !unit )
	continue;
      if( unit->type != Unit::Single )
	continue;

      // Number of singles still needed to build a triple
      int needed = 2;

      std::vector<Action> actions;

      bool possible = true;
      for( int otherRow = row + 1; otherRow < Field::Rows; ++otherRow ) {

	Unit* otherUnit = table.get(col, otherRow);
	if( !otherUnit )
	  continue;

	if( otherUnit->type != Unit::Single ) {
	      
	  // We would have to remove an "expensive" unit
	  // Dont do that for now...
	  // TODO: Maybe assign cost instead
	  possible = false;
	  break;
	}

	if( otherUnit->color == unit->color ) {

	  needed--;
	}
	else {

	  actions.push_back(Action());
	  actions.back().type = Action::RemoveUnit;
	  actions.back().srcPos = Position(col, otherRow);
	}
      }

      if( !possible )
	continue;

      if( needed <= 0 ) {

	moves.push_back(Move(actions));
      }

      // At this point, we need to look at the other cols
      // for singles to drop onto this 

      std::map<int, std::vector< std::vector<Action> > > allColActions;
	
      for( int otherCol = 0; otherCol < Field::Cols; ++otherCol ) {
	  
	if( otherCol == col )
	  continue; // We are already done with this one...

	std::vector<Action> colActions;
	int colNeeded = needed;

	for( int otherRow = Field::Rows - 1; (colNeeded > 0) && (otherRow >= 0); --otherRow ) {
	    
	  Unit* otherUnit = table.get(otherCol,  otherRow);
	  if( !otherUnit )
	    continue;

	  if( otherUnit->type != Unit::Single ) {

	    // Dont' remove expensive units, see above
	    break;
	  }
	  if( otherUnit->color == unit->color ) {

	    // Store a copy of the current actions and continue to find more 
	    // singles...

	    colActions.push_back(Action());
	    colActions.back().type = Action::MoveUnit;
	    colActions.back().srcPos = Position(otherCol, otherRow);
	    colActions.back().dstCol = col;

	    colNeeded--;
	    int yield = needed - colNeeded;
	    allColActions[ yield ].push_back(colActions);
	  }
	  else {
	      
	    colActions.push_back(Action());
	    colActions.back().type = Action::RemoveUnit;
	    colActions.back().srcPos = Position(otherCol, otherRow);
	  }
	}
      }

      // Now we have all possible continuations in allColActions

      if( allColActions.empty() )
	continue; // Triple cannot be completed 

      if( needed == 1 ) {
	  
	// Select cheapest
	std::vector<Action> cheapest;
	for( std::vector<std::vector<Action> >::const_iterator it = allColActions[1].begin();
	     it != allColActions[1].end(); ++it ) {
	    
	  if( cheapest.empty() || cheapest.size() > (*it).size() ) {
	      
	    cheapest = *it;
	  }
	}

	// Append actions, complete move
	actions.insert( actions.end(), cheapest.begin(), cheapest.end() );
	moves.push_back(Move(actions));
	continue;
      }
      else {

	assert(needed == 2);

	std::vector<Action> cheapest;
	  
	// Find pair-wise cheapeast
	if( allColActions[1].size() >= 2 ) {
	    
	  std::vector<Action> cheapestFirst = allColActions[1].front();
	  std::vector<Action> cheapestSecond;
	    
	  for( unsigned i = 1; i < allColActions[1].size(); ++i ) {
	      
	    const std::vector<Action> tmp = allColActions[1][i];
	      
	    if( cheapestSecond.empty() ) {
		
	      cheapestSecond = tmp;
	      continue;
	    }
	      
	    std::vector<Action>& maxVector = (cheapestFirst.size() > cheapestSecond.size()) ? cheapestFirst : cheapestSecond;
	      
	    if( tmp.size() < maxVector.size() )
	      maxVector = tmp;
	  }

	  cheapest.insert(cheapest.end(), cheapestFirst.begin(), cheapestFirst.end() );
	  cheapest.insert(cheapest.end(), cheapestSecond.begin(), cheapestSecond.end() );
	}

	// Now compare with cheapest 2-from-1-column action
	for( std::vector<std::vector<Action > >::const_iterator it = allColActions[2].begin();
	     it != allColActions[2].end(); ++it ) {

	  if( cheapest.empty() || cheapest.size() > (*it).size() ) {

	    cheapest = *it;
	  }
	}

	if( cheapest.empty() )
	  continue; // Triple can not be completed

	// Append actions and complete move
	actions.insert( actions.end(), cheapest.begin(), cheapest.end() );
	moves.push_back(Move(actions));
	continue;
      }
    }
  }
}
