#ifndef _HEALTHSCENENODE_H_
#define _HEALTHSCENENODE_H_

#include "scenenode.h"

class Field;

class HealthSceneNode : public SceneNode
{
public:
  HealthSceneNode(Field* field, int y);

  void update();
  void render();

  Field* field;
  int y;
  int health;
  int ticks_;
};

#endif /* _HEALTHSCENENODE_H_ */
