#include "cputurnstate.h"
#include "globals.h"

#include <stm/statemanager.h>

#include <cassert>

CpuTurnState::CpuTurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm)
  : TurnState(field, otherField, scene, stm)
{
  setName("CpuTurnState");
}

CpuTurnState::~CpuTurnState()
{
}

void CpuTurnState::update()
{
  if( getTransitionState() != TransitionInDone )
    return;

  updateActionQueue(actions, field, otherField);
  // Still busy
  if( !actions.empty() )
    return;

  // Think...
  if( moves > 0 ) {

    moves -= 1;

    cpu.makeMoveActions(actions, field, otherField, scene);
    if( actions.empty() ) {

      getStateManager()->popState();
    }
  }
  else {

    getStateManager()->popState();
  }
}

void CpuTurnState::render()
{
  g_font->print( TextAreaX, field->getY0() + 20, "MOVES: %d", moves );
}

void CpuTurnState::onTransition(STM::State* /*previousState*/)
{
  if( getTransitionState() == TransitionIn ) {

    if( getStateManager()->isQueueEmptyAfterTransition() ) {

      field->onStartOfTurn();
      field->checkAttack(otherField, actions);
    }

    setTransitionState(TransitionInDone);
  }
  else {

    setTransitionState(TransitionOutDone);
  }
}
