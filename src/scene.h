#ifndef _SCENE_H_
#define _SCENE_H_

#include <vector>

class SceneNode;

class Scene
{
public:
  Scene();
  ~Scene();

  void addNode( SceneNode* node );
  void removeNode( SceneNode* node );

  void update();
  void render();
  
  std::vector<SceneNode*> nodes;
};

#endif /* _SCENE_H_ */
