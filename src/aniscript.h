#ifndef _ANISCRIPT_H_
#define _ANISCRIPT_H_

#include <functional> // function
#include <list>
#include <memory> // shared_ptr
#include <vector>

class SceneNode;

namespace ani
{
  class Context;
  class Script;

  class CommandImpl
  {
  public:
    virtual ~CommandImpl() {}
    virtual bool isFinished(const Context* ctx) const = 0;
    virtual void setup(Context* ctx, SceneNode* node) const = 0;
    virtual void cleanup(Context* ctx, SceneNode* node) const = 0;
    virtual void update(Context* ctx, SceneNode* node) const = 0;
  };

  class Command
  {
  public:
    Command(CommandImpl* impl);

    bool isFinished(const Context* ctx) const;
    void setup(Context* ctx, SceneNode* node) const;
    void cleanup(Context* ctx, SceneNode* node) const;
    void update(Context* ctx, SceneNode* node) const;

  private:
    std::shared_ptr<CommandImpl> impl_;
  };

  class Script
  {
  public:
    class Iterator
    {
    public:
      Iterator(const Script* script)
        : script_(script)
      {
	it_ = script->commands_.begin();
      }
    
      bool atEnd() const
      {
	return it_ == script_->commands_.end();
      }

      bool hasNext()
      {
	return !atEnd();
      }

      void next()
      {
	++it_;
      }

      const Command& getCommand() const
      {
	return *it_;
      }

    private:
      const Script* script_;
      std::list<Command>::const_iterator it_;
    };

    Script(SceneNode* node = 0);
    Script(const Command& cmd);
    Script(CommandImpl* cmd);
    ~Script();

    Script& operator<<( const Script& rhs );

    Script operator&(const Script& rhs) const;
    Script& operator&=(const Script& rhs);

  private:
    std::list<Command> commands_;
  };

  class Context
  {
  public:
    Context(const Script* script, SceneNode* node = 0);
    ~Context();

    template<typename T>
    void push(const T val)
    {
      push(reinterpret_cast<const char*>(&val), static_cast<int>(sizeof(T)));
    }
    
    template<typename T>
    T pop()
    {
      T val;
      pop(reinterpret_cast<char*>(&val), static_cast<int>(sizeof(T)));
      return val;
    }

    template<typename T>
    T peek(int offset = 0) const
    {
      T val;
      peek(reinterpret_cast<char*>(&val), static_cast<int>(sizeof(T)), offset);
      return val;
    }

    bool isFinished() const;
    bool areChildrenFinished() const;

    void update();
    void createChild(const Script* script);
    void yield();
    void setNode(SceneNode* node);

  private:    
    void push(const char* data, int size);
    void pop(char* data, int size);
    void peek(char* data, int size, int offset = 0) const;
    
    enum {
      StackSize  = 100,
    };

    const Script* script_;
    Script::Iterator commandIterator_;
    SceneNode* node_;
    char stackData_[StackSize];
    int stackIndex_;
    std::list<Context*> children_;
    bool yielded_;
  };

  typedef std::function<float(float, float, float)> InterpolateFunctor;

  float interpolateLinear(const float a, const float b, const float arg);
  float interpolateSmoothstep(const float a, const float b, const float arg);

  Script moveBy(const float dx, const float dy, const int frames, const InterpolateFunctor& interpolate);
  Script moveTo(const float x, const float y, const int frames, const InterpolateFunctor& interpolate);

  Script setAlpha(const float alpha, const int frames, const InterpolateFunctor& interpolate);

  Script resetAlphaOsc(SceneNode* node = 0);

  Script kill(SceneNode* node = 0);
  Script join();

  Script wait(const int frames);
  Script setVisible(SceneNode* node, bool visible);

  Script waitForSpriteAnim();

  Script startBlink();
  Script stopBlink();

} // ns ani

#endif /* _ANISCRIPT_H_ */
