#include "scenenode.h"
#include "tilemap.h"
#include "tilemaprenderer.h"

#include <tmepp/video.h>

#include <cmath>

TileMapRenderer::TileMapRenderer(TileMap* map)
  : map_(map)
{
}

void TileMapRenderer::render(SceneNode* node)
{
  if( !map_ )
    return;
  
  int i = 0;
  int y = roundf(node->pos.y);
  for( int row = 0; row < map_->getNumRows(); ++row ) {
    
    int x = roundf(node->pos.x);
    for( int col = 0; col < map_->getNumCols(); ++col ) {
      
      const int spriteID = map_->get(i++);
      if( spriteID > 0 ) { // FIXME: 0 is a valid sprite ID
	  
	Tme::Video::blitSprite( spriteID, 
				x, y, 
				static_cast<unsigned char>(roundf(node->alpha * 255.0f)) );
      }

      x += map_->getTileWidth();
    }

    y += map_->getTileHeight();
  }
}

