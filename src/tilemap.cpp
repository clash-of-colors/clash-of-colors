#include "tilemap.h"

#include <cstring>

TileMap::TileMap(const int numCols, const int numRows, const int tileWidth, const int tileHeight)
  : numCols_(numCols)
  , numRows_(numRows)
  , tileWidth_(tileWidth)
  , tileHeight_(tileHeight)
  , tiles_( new TileType[numCols * numRows] )
{
  memset(tiles_, 0, sizeof(TileType) * numCols * numRows);
}

TileMap::~TileMap()
{
  delete [] tiles_;
}
