#ifndef _SPRITERENDERER_H_
#define _SPRITERENDERER_H_

#include "scenenoderenderer.h"
#include "sprite.h"

class SpriteRenderer : public SceneNodeRenderer
{
public:

  void setSprite( const Sprite& frame );

  void render(SceneNode* node) override;
  
private:
  Sprite sprite_;
};

#endif /* _SPRITERENDERER_H_ */
