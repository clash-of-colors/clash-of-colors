#include "field.h"
#include "globals.h"
#include "movecursorstate.h"
#include "moveunitstate.h"
#include "playerturnstate.h"
#include "unit.h"

#include <stm/statemanager.h>

#include <stdlib.h> // rand

#include <cassert>
#include <iostream>

TurnState::TurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm)
  : State(stm)
  , field(field)
  , otherField(otherField)
  , scene(scene)
  , moves(0)
{
  setName("TurnState");
}

TurnState::~TurnState()
{
}

void TurnState::setup()
{
  moves = 3;
}
