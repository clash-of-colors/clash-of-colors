#ifndef _UNIT_H_
#define _UNIT_H_

#include "position.h"
#include "scenenode.h"
#include "spriteanim.h"

#include <tmepp/spritefont.h>

class SpriteRenderer;

class Unit : public SceneNode
{
public:
  enum Type {

    Single,
    Quad,
    TripleCharging,
    QuadCharging,
    Wall,
    NumTypes,
  };

  enum Color {

    Yellow,
    Red,
    Blue,
    NumColors,
  };
  
  Unit(Type type, Color color);
  ~Unit();

  void update() override;
  void render() override;

  void setType(const Type type);

  //bool isHeavy() const;
  int getWeight() const;

  Position getCenterPos() const;

  Type type;
  Color color;
  Position fieldPos;

  int width;
  int height;

  int turnsToDeploy;
  int health;
  int visibleHealth;

  spani::Runner animRunner_;
  SpriteRenderer* spriteRenderer_;

private:
  Tme::SpriteFont* getSmallFont() const;
};

#endif /* _UNIT_H_ */
