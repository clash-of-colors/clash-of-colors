#include "runscriptaction.h"

RunScriptAction::RunScriptAction(const ani::Script& script)
  : script_(script)
  , context_(&script_)
{
}

bool RunScriptAction::isDone() const
{
  return context_.isFinished(); 
}

void RunScriptAction::update()
{
  context_.update();
}

