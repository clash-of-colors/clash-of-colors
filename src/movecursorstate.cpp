#include "action.h"
#include "aniscript.h"
#include "cursorscenenode.h"
#include "field.h"
#include "fps.h"
#include "globals.h"
#include "ingamemenu.h"
#include "menustate.h"
#include "movecursorstate.h"
#include "moveunitstate.h"
#include "puff.h"
#include "runscriptaction.h"
#include "scene.h"
#include "unit.h"

#include <stm/statemanager.h>

#include <algorithm>
#include <cassert>
#include <iostream>

MoveCursorState::MoveCursorState(ActionQueue& actions, Field* field, Scene* scene, STM::StateManager* stm)
  : State(stm)
  , actions(actions)
  , field(field)
  , scene(scene)
  , moveUnitState(new MoveUnitState(actions, field, scene, stm))
  , endTurn(false)
  , cursor(0)
  , vpadRepeater_(fps::seconds(0.1f))
{
  setName("MoveCursorState");
}

MoveCursorState::~MoveCursorState()
{
  delete moveUnitState;
}

void MoveCursorState::update()
{
  updateActionQueue(actions, field, nullptr);

  if( getTransitionState() != TransitionInDone )
    return;

  if( actions.empty() && !cursor ) {

    getStateManager()->popState();
  }
}

bool MoveCursorState::onInput(VPAD* vpad)
{
  vpadRepeater_.update(vpad);

  if( vpad->justPressed(VPAD::ButtonStart) ) {

    getStateManager()->pushState( new MenuState(getStateManager(), InGameMenu(), scene), true );
    return true;
  }

  if( !actions.empty() || !cursor )
    return true;

  // DIRECTIONS
  //   Move cursor
  // KILL
  //   Remove unit and pop
  // REINFORCE
  //   Drop new units and pop
  // PICK
  //   Take unit and go to MoveUnitState
  // END TURN
  //   Double pop (Back to game state)
  // MENU
  //   TODO...
  
  int dx = cursor->unit ? cursor->unit->width : 1;
  int dy = cursor->unit ? cursor->unit->height : 1;

  if( vpadRepeater_.justPressed(VPAD::ButtonLeft) ) {
    if( cursor->fieldPos.x > 0 ) {

      --cursor->fieldPos.x;
    }
    updateCursor();
  }

  if( vpadRepeater_.justPressed(VPAD::ButtonRight) ) {
    if( cursor->fieldPos.x + dx < Field::Cols ) {

      cursor->fieldPos.x += dx;
    }
    updateCursor();
  }

  if( vpadRepeater_.justPressed(VPAD::ButtonUp) ) {
    if( cursor->fieldPos.y > 0 ) {
      
      --cursor->fieldPos.y;
    }
    updateCursor();
  }

  if( vpadRepeater_.justPressed(VPAD::ButtonDown) ) {
    if( cursor->fieldPos.y + dy < Field::Rows ) {
      
      cursor->fieldPos.y += dy;
    }
    updateCursor();
  }

  if( vpad->justPressed(VPAD::ButtonY) ) {

    Unit* unit = cursor->unit;

    if( !unit ) {
      std::cerr << "No unit" << std::endl;
      return true;
    }

    std::vector<Unit*> units;
    field->getUnitsInCols(unit->fieldPos.x, unit->width, units);
    for( auto const otherUnit : units ) {

      if(otherUnit == unit)
	continue;

      if(otherUnit->fieldPos.y > unit->fieldPos.y) {
	std::cerr << "Unit is blocked" << std::endl;
	return true;
      }
    }

    cursor->setUnit(0);
    cursor->visible = false;

    unit = field->takeUnit(unit);
    assert(unit);

    moveUnitState->setup(unit);
    unit = 0;

    getStateManager()->pushState(moveUnitState);

    return true;
  }

  if( vpad->justPressed(VPAD::ButtonX) ) {

    ani::Script script;

    const float screenY0 = 240;

    for( int i = 0; i < 5; ++i ) {
	
      Unit::Type type = (Unit::Type) ((rand() % 10) ? Unit::Single : Unit::Quad);
      Unit::Color color = (Unit::Color) (rand() % 3);

      Unit* unit = new Unit(type, color);

      int col = rand() % (Field::Cols + 1 - unit->width);
	
      int j = 0;
      for( ; j < Field::Cols; ++j, col = (col + 1) % Field::Cols ) {

	if( field->canDropUnit(unit, col) ) {
	    
	  field->dropUnit(unit, col);

	  const float screenX = FieldX0 /*+ Field::CellMarginX*/ + unit->fieldPos.x * Field::CellW;
	  const float screenY1 = FieldY1 /*+ Field::CellMarginY*/ + unit->fieldPos.y * Field::CellH;
	    
	  unit->pos.x = screenX;
	  unit->pos.y = screenY0;
	  scene->addNode(unit);

	  script << ani::Script(unit) 
		 << ani::moveTo( screenX, screenY1, 
				 fps::distanceVelocity(screenY1 - screenY0, MoveVelocity),
				 ani::interpolateSmoothstep );
	  break;
	}
      }
      if( j == Field::Cols ) {

	std::cerr << "Could not drop unit..." << std::endl;
	delete unit;
      }
    }
      
    std::cerr << "Action: Drop reinforcements" << std::endl;
    actions.push(ActionPtr(new RunScriptAction(script)));
    destroyCursor(); // Give up control...

    return true;
  }

  if( vpad->justPressed(VPAD::ButtonB) ) {

    Unit* unit = cursor->unit;

    if( !unit ) {
      std::cerr << "No unit" << std::endl;
      return true;
    }

    cursor->setUnit(0);
      
    unit = field->takeUnit(unit);
    assert(unit);

    unit->active = false;
    unit->visible = false;
      
    SceneNode* puff = makePuff(unit->getCenterPos());
    scene->addNode(puff);

    ani::Script script(puff);
    script << ani::waitForSpriteAnim()
	   << ani::kill();

    actions.push(ActionPtr(new RunScriptAction(script)));

    destroyCursor(); // Give up control

    return true;
  }
  
  if( vpad->justPressed(VPAD::ButtonA) ) {

    destroyCursor();
    endTurn = true;
    return true;
  }

  return true;
}

void MoveCursorState::setup(int col)
{
  assert(!cursor);
  cursor = new CursorSceneNode();
  scene->addNode(cursor);
  cursor->fieldPos.x = col;
  cursor->fieldPos.y = Field::Rows - 1;
  updateCursor();

  endTurn = false;
}

void MoveCursorState::updateCursor()
{
  Unit* unit = field->getUnitAt(cursor->fieldPos);
  if( unit ) {
    
    // Align cursor to unit
    cursor->fieldPos = unit->fieldPos;
  }

  cursor->setUnit(unit);
  cursor->pos.x = FieldX0 + cursor->fieldPos.x * Field::CellW;
  cursor->pos.y = FieldY1 + cursor->fieldPos.y * Field::CellH;
  cursor->visible = true;
}

void MoveCursorState::destroyCursor()
{
  cursor->setUnit(0);
  cursor->visible = false;
  cursor->active = false;
  cursor = 0;
}

void MoveCursorState::onTransition(STM::State* prevState)
{
  if( getTransitionState() == TransitionOut ) {
    
    setTransitionState(TransitionOutDone);
    return;
  }
  
  if( getStateManager()->isQueueEmptyAfterTransition() ) {
    
    if( prevState == moveUnitState ) {
 
      destroyCursor();
    }
  }
  
   setTransitionState(TransitionInDone);
}
