#ifndef _BLINKER_H_
#define _BLINKER_H_

#include "component.h"

class Blinker : public Component
{
public:
  Blinker(const float frequency);
  void start();
  void stop();
  void update(SceneNode* node) override;

private:
  int interval_;
  int ticks_;
};

#endif /* _BLINKER_H_ */
