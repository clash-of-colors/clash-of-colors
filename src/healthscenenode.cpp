#include "field.h"
#include "fps.h"
#include "globals.h"
#include "healthscenenode.h"

HealthSceneNode::HealthSceneNode(Field* field, int y)
  : field(field)
  , y(y)
  , health(field->health)
  , ticks_(0)
{
}

void HealthSceneNode::update()
{
  const int HealthPerSecond = 15;
  const int FramesPerHealth = std::max(1, FPS / HealthPerSecond);

  if( field->health != health ) {

    ticks_++;
  }

  if( field->health < health && ticks_ >= FramesPerHealth ) {

    ticks_ = 0;
    health--;
  }
  else if( field->health > health  && ticks_ >= FramesPerHealth ) {

    ticks_ = 0;
    health++;
  }

  if( field->health != health ) {

    ticks_++;
  }
  else {

    ticks_ = 0;
  }
}

void HealthSceneNode::render()
{
  if( health > field->health ) {

    g_fontRed->print(TextAreaX, y, "HP:    %d", health);
  }
  else {
    
    g_font->print(TextAreaX, y, "HP:    %d", health);
  }
}
