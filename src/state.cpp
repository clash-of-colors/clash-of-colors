#include "state.h"

#include <cassert>

State::State(STM::StateManager* stm) 
  : STM::State(stm)
{
}

void State::onTransition(STM::State* /*previousState*/)
{
  switch( (int) getTransitionState() ) {

  case TransitionIn:
    setTransitionState(TransitionInDone);
    break;
  case TransitionOut:
    setTransitionState(TransitionOutDone);
    break;
  default:
    assert(false);
  }
}

bool State::onInput(VPAD* /*vpad*/)
{
  return false;
}
