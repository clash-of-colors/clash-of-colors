#ifndef _UNITTABLE_H_
#define _UNITTABLE_H_

#include "field.h"

class UnitTable
{
public:
  enum {
    Rows = Field::Rows * 2
  };

  UnitTable(const std::vector<Unit*>& units);
    
  void set(const int col, const int row, Unit* unit);
  Unit* get(const int col, const int row) const;
  void setUnit(Unit* unit);
  void resetUnit(Unit* unit);

private:
  void setUnit(Unit* unit, Unit* value);

  Unit* units_[ Field::Cols * Rows ];
};

#endif /* _UNITTABLE_H_ */
