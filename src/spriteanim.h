#ifndef _SPRITEANIM_H_
#define _SPRITEANIM_H_

#include "sprite.h"

#include <memory>
#include <vector>

namespace spani
{
  class Loop;
  class Runner;
  
  class Step
  {
  public:
    virtual ~Step();
    virtual void setup(Runner* runner) const;
    virtual void execute(Runner* runner) const = 0;
  };

  typedef std::shared_ptr<Step> StepPtr;

  StepPtr frame(const int id, const float dx = 0, const float dy = 0);
  StepPtr wait(const int n);
  StepPtr halt();
  
  class Anim
  {
  public:
    Anim(const int fps);
    Anim& operator<<( StepPtr step );
    Anim& operator<<( const Loop& frame );
    
  private:
    friend class Runner;
    std::vector<StepPtr> steps_;
    int fps_;
  };

  class Loop
  {
  public:
    Loop& operator<<( StepPtr step );
    
  private:
    friend class Anim;
    std::vector<StepPtr> steps_;
  };

  class Runner
  {
  public:
    Runner(const int fps);

    void setAnim(const Anim* anim);
    void update();
    bool isFinished() const;

    void setSprite(const Sprite& sprite);
    const Sprite& getSprite() const;

    void setCounter(const int index);
    int getCounter() const;
    void jump(const int index);
    void yield();
    void halt();

  private:
    void next();

    int fps_;
    const Anim* anim_;
    int index_;
    int counter_;
    bool yielded_;
    Sprite sprite_;
    int frameTicks_;
    int frameDelay_;
  };

} // ns spani

#endif /* _SPRITEANIM_H_ */
