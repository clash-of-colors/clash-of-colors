#include "componentscenenode.h"
#include "puff.h"
#include "spriteanim.h"
#include "spriteanimrenderer.h"
#include "sprites.h"

namespace 
{

const int PuffSize = 14;
  
const spani::Anim A_PUFF = spani::Anim(20)
  << spani::frame(SPRITE_PUFF_0)
  //<< spani::frame(SPRITE_PUFF_1, 1, 1)
  << spani::frame(SPRITE_PUFF_2, 2, 2)
  //<< spani::frame(SPRITE_PUFF_3, 3, 3)
  << spani::frame(SPRITE_PUFF_4, 4, 4)
  << spani::frame(SPRITE_PUFF_5, 5, 5)
  << spani::frame(SPRITE_PUFF_6, 6, 6);

} // ns 

SceneNode* makePuff(const Position& pos)
{
  SpriteAnimRenderer* renderer = new SpriteAnimRenderer;
  renderer->setAnim(&A_PUFF);

  ComponentSceneNode* node = new ComponentSceneNode;
  node->pos.x = pos.x - PuffSize / 2;
  node->pos.y = pos.y - PuffSize / 2;
  node->setRenderer(renderer);

  return node;
}
