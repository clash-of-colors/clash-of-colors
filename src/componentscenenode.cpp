#include "component.h"
#include "componentscenenode.h"
#include "scenenoderenderer.h"

#include <cassert>

ComponentSceneNode::ComponentSceneNode()
  : renderer_(0)
{
  components_.reserve(8);
}

ComponentSceneNode::~ComponentSceneNode()
{
  delete renderer_;

  for( auto c : components_ ) {

    delete c;
  }
}

void ComponentSceneNode::render()
{
  assert(renderer_);
  renderer_->render(this);
}

void ComponentSceneNode::update()
{
  for( auto component : components_ ) {

    component->update(this);
  }
}

void ComponentSceneNode::setRenderer(SceneNodeRenderer* renderer)
{
  delete renderer_;
  renderer_ = renderer;
}

SceneNodeRenderer* ComponentSceneNode::getRenderer() const
{
  return renderer_;
}

void ComponentSceneNode::addComponent(Component* component)
{
  components_.push_back(component);
}
