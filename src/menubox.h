#ifndef _MENUBOX_H_
#define _MENUBOX_H_

#include "componentscenenode.h"
#include "sprites.h"

class TileMap;

class MenuBox : public ComponentSceneNode
{
public:
  MenuBox(const Position& pos, const int w, const int h, const int spriteID = SPRITE_MENU_8X8_0);
  ~MenuBox();

  int w_;
  int h_;

private:
  TileMap* map_;
};

#endif /* _MENUBOX_H_ */
