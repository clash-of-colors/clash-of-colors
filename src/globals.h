#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include "field.h"
#include "globals.h"

#include <tmepp/spritefont.h>

#undef COC_WIDESCREEN
#if defined(COC_PLATFORM_PC) || defined(COC_PLATFORM_PANDORA)
//#define COC_WIDESCREEN 1
#endif

#if defined(COC_WIDESCREEN)
static const int ScreenW = 400;
#else
static const int ScreenW = 320;
#endif

static const int ScreenH = 240;

static const int TextAreaW = 100;

static const int FieldHeight = Field::Rows * Field::CellH;
static const int FieldWidth = Field::Cols * Field::CellW;

static const int GapW = (ScreenW - FieldWidth - TextAreaW) / 3;

static const int FieldX0 = GapW;
static const int FieldY0 = (ScreenH - FieldHeight * 2 - 8) / 2;
static const int FieldY0B = FieldY0 + FieldHeight;
static const int FieldY1 = FieldY0 + FieldHeight + 8;

static const int TextAreaX = FieldWidth + GapW * 2;

static const float MoveVelocity = 2.0f * 60.0f; // Pixels/second

extern Tme::SpriteFont* g_font;
extern Tme::SpriteFont* g_fontRed;

extern Tme::SpriteFont* g_fontSmallNumWhite;
extern Tme::SpriteFont* g_fontSmallNumRed;
extern Tme::SpriteFont* g_fontSmallNumGreen;
extern Tme::SpriteFont* g_fontSmallNumBlack;

#endif /* _GLOBALS_H_ */
