#include "vpad.h"

struct PCVPADAdapter : public VPADAdapter
{
  VPAD* vpad;

  PCVPADAdapter(VPAD* vpad)
    : vpad(vpad)
  {
  }
  
  void onKey(const SDLKey key, const bool pressed)
  {
    switch( key ) {
    case SDLK_LEFT:
      vpad->setPressed(VPAD::ButtonLeft, pressed);
      break;
    case SDLK_RIGHT:
      vpad->setPressed(VPAD::ButtonRight, pressed);
      break;
    case SDLK_UP:
      vpad->setPressed(VPAD::ButtonUp, pressed);
      break;
    case SDLK_DOWN:
      vpad->setPressed(VPAD::ButtonDown, pressed);
      break;
    case SDLK_w:
      vpad->setPressed(VPAD::ButtonX, pressed);
      break;
    case SDLK_a:
      vpad->setPressed(VPAD::ButtonY, pressed);
      break;
    case SDLK_s:
      vpad->setPressed(VPAD::ButtonB, pressed);
      break;
    case SDLK_d:
      vpad->setPressed(VPAD::ButtonA, pressed);
      break;
    case SDLK_ESCAPE:
      vpad->setPressed(VPAD::ButtonSelect, pressed);
      break;
    case SDLK_RETURN:
      vpad->setPressed(VPAD::ButtonStart, pressed);
      break;
    default:
      break;
    }
  }

  void onSDLEvent(SDL_Event* event) override
  {
    if(event->type == SDL_KEYDOWN) {
      onKey(event->key.keysym.sym, true);
    }
    else if(event->type == SDL_KEYUP) {
      onKey(event->key.keysym.sym, false);
    }
  }
};

VPADAdapter* sysCreateVPADAdapter(VPAD* vpad)
{
  return new PCVPADAdapter(vpad);
}
