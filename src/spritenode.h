#ifndef _SPRITENODE_H_
#define _SPRITENODE_H_

#include "scenenode.h"

class SpriteNode : public SceneNode
{
public:
  SpriteNode(const int spriteID);
  void render() override;

private:
  int spriteID_;
};

#endif /* _SPRITENODE_H_ */
