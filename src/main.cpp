#include "fps.h"
#include "gamestate.h"
#include "globals.h"
#include "sprites.h"
#include "titlestate.h"

#include <tmepp/tmepp.h>
#include <tmepp/video.h>

#include <stm/statemanager.h>
#include <stm/state.h>

#include <SDL.h>

#include <cassert>

Tme::SpriteFont* g_font(0);
Tme::SpriteFont* g_fontRed(0);
Tme::SpriteFont* g_fontSmallNumWhite(0);
Tme::SpriteFont* g_fontSmallNumRed(0);
Tme::SpriteFont* g_fontSmallNumGreen(0);
Tme::SpriteFont* g_fontSmallNumBlack(0);

int main( int /*argc*/, char** /*argv*/ )
{
  VPAD vpad;
  std::unique_ptr<VPADAdapter> vpadAdapter(sysCreateVPADAdapter(&vpad));

#if defined(COC_PLATFORM_WIZ)
  assert( 0 == SDL_Init(SDL_INIT_JOYSTICK) );
  assert(SDL_JoystickOpen(0));
  assert(Tme::Video::init( Tme::VideoWiz, ScreenW, ScreenH, false ));
#elif defined(COC_PLATFORM_PC)
  assert(Tme::Video::init( Tme::VideoGL, ScreenW * 2, ScreenH * 2, false, 2 ));
#else
  #error Unknown platform
#endif

  {
    STM::StateManager stm;
    stm.pushState(new TitleState(&stm), true);
    
    Uint32 ticks0 = SDL_GetTicks();

    Tme::Spriteset* sprites = Tme::loadSpriteset("../data/sprites.ssx");
    assert(sprites);
    sprites->registerSprites(0);

    Tme::SpriteFont font(SPRITE_FONT_00, 10, 10, 60);
    g_font = &font;

    Tme::SpriteFont fontRed(SPRITE_REDFONT_00, 10, 10, 60);
    g_fontRed = &fontRed;

    Tme::SpriteFont fontSmallNumWhite(SPRITE_SMALLNUM_WHITE_0, 4, 6, 10, '0');
    g_fontSmallNumWhite = &fontSmallNumWhite;
    Tme::SpriteFont fontSmallNumRed(SPRITE_SMALLNUM_RED_0, 4, 6, 10, '0');
    g_fontSmallNumRed = &fontSmallNumRed;
    Tme::SpriteFont fontSmallNumGreen(SPRITE_SMALLNUM_GREEN_0, 4, 6, 10, '0');
    g_fontSmallNumGreen = &fontSmallNumGreen;
    Tme::SpriteFont fontSmallNumBlack(SPRITE_SMALLNUM_BLACK_0, 4, 6, 10, '0');
    g_fontSmallNumBlack = &fontSmallNumBlack;

    Uint32 ticks = 0;
   
    bool run = true;
    while( run && !stm.isDone() ) {
      
      Tme::Video::startFrame();
      Tme::Video::clear();

      stm.render();

      Tme::Video::renderFrame();
      Tme::Video::finishFrame();

      const Uint32 PERIOD = 1000 / FPS;
      Uint32 ticks1 = SDL_GetTicks();
      Uint32 dt = ticks1 - ticks0;
      if( dt < PERIOD ) {
	
	SDL_Delay(PERIOD - dt);
	ticks1 = SDL_GetTicks();
	dt = ticks1 - ticks0;
      }
      ticks0 = ticks1;

      ticks += dt;

      SDL_Event event;
      while(SDL_PollEvent(&event)) {

	if( event.type == SDL_QUIT ) {
	  
	  run = false;
	  break;
	}
	else {

	  vpadAdapter->onSDLEvent(&event);
	}
      }
      
      while( ticks >= PERIOD ) {
	ticks -= PERIOD;

	for( int i = 0;; ++i ) {

	  State* state = dynamic_cast<State*>(stm.getState(i));
	  if( !state || state->onInput(&vpad) )
	    break;
	}

	vpad.update();
	stm.update();
      }
    }
  }
  Tme::Video::shutdown();

  return 0;
}
