#ifndef _TILEMAP_H_
#define _TILEMAP_H_

typedef short TileType;

class TileMap
{
public:
  TileMap(const int numCols, const int numRows, const int tileWidth, const int tileHeight);
  ~TileMap();

  void set(const int i, const TileType value)
  {
    tiles_[i] = value;
  }

  void set(const int col, const int row, const TileType value)
  {
    tiles_[col + row * numCols_] = value;
  }

  TileType get(const int i) const
  {
    return tiles_[i];
  }
  
  TileType get(const int col, const int row) const
  {
    return tiles_[col + row * numCols_];
  }

  int getNumCols() const
  {
    return numCols_;
  }

  int getNumRows() const
  {
    return numRows_;
  }

  int getTileWidth() const
  {
    return tileWidth_;
  }

  int getTileHeight() const
  {
    return tileHeight_;
  }

private:
  int numCols_;
  int numRows_;
  int tileWidth_;
  int tileHeight_;
  TileType* tiles_;
};

#endif /* _TILEMAP_H_ */
