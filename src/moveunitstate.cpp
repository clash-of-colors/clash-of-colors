#include "action.h"
#include "aniscript.h"
#include "fps.h"
#include "globals.h"
#include "ingamemenu.h"
#include "menustate.h"
#include "moveunitstate.h"
#include "runscriptaction.h"
#include "unit.h"

#include <stm/statemanager.h>

#include <cassert>
#include <cstddef> // nullptr
#include <iostream>

MoveUnitState::MoveUnitState(ActionQueue& actions, Field* field, Scene* scene, STM::StateManager* stm) 
  : State(stm)
  , actions(actions)
  , field(field)
  , scene(scene)
  , unit(0)
  , vpadRepeater_(fps::seconds(0.1f))
{
  setName("MoveUnitState");
}

MoveUnitState::~MoveUnitState()
{
  assert(!unit);
}

bool MoveUnitState::onInput(VPAD* vpad)
{
  vpadRepeater_.update(vpad);

  if( vpad->justPressed(VPAD::ButtonStart) ) {

    getStateManager()->pushState( new MenuState(getStateManager(), InGameMenu(), scene), true );
    return true;
  }

  if( !actions.empty() || !unit )
    return true;

  if( vpadRepeater_.justPressed(VPAD::ButtonLeft) ) {
    if( unit->fieldPos.x > 0 ) {

      unit->fieldPos.x--;
      unit->pos.x -= Field::CellW;
    }
  }

  if( vpadRepeater_.justPressed(VPAD::ButtonRight) ) {
    if( unit->fieldPos.x + unit->width < Field::Cols ) {

      unit->fieldPos.x++;
      unit->pos.x += Field::CellW;
    }
  }

  if( vpad->justPressed(VPAD::ButtonY) ) {
    if( field->canDropUnit( unit, unit->fieldPos.x ) ) {

      field->dropUnit( unit, unit->fieldPos.x );

      const float y = FieldY1 /*+ Field::CellMarginY*/ + unit->fieldPos.y * Field::CellH;

      ani::Script script(unit);
      script << ani::moveTo( unit->pos.x,
			     y,
			     fps::distanceVelocity(y - unit->pos.y, MoveVelocity),
			     ani::interpolateSmoothstep );

      std::cerr << "Action: Manual drop" << std::endl;
      actions.push(ActionPtr(new RunScriptAction(script)));

      unit = 0;
    }
  }

  return true;
}

void MoveUnitState::update()
{
  if( getTransitionState() != TransitionInDone )
    return;

  updateActionQueue(actions, field, nullptr);
  
  if( actions.empty() && !unit ) {
    
    getStateManager()->popState();
  }
}

void MoveUnitState::render()
{
}

void MoveUnitState::setup(Unit* unit)
{
  assert( !this->unit );
  this->unit = unit;
  
  const float y = FieldY1 + FieldHeight + 4;

  ani::Script script(unit);
  script << ani::moveTo( unit->pos.x,
			 y,
			 fps::distanceVelocity(y - unit->pos.y, MoveVelocity),
			 ani::interpolateSmoothstep );

  std::cerr << "Action: Move unit" << std::endl;
  actions.push(ActionPtr(new RunScriptAction(script)));;
}
