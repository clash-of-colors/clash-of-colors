#ifndef _CPUTURNSTATE_H_
#define _CPUTURNSTATE_H_

#include "action.h"
#include "cpu.h"
#include "turnstate.h"

class CpuTurnState : public TurnState
{
public:
  CpuTurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm);
  ~CpuTurnState();
  
  void update() override;
  void render() override;

  void onTransition(STM::State* previousState) override;

private:
  Cpu cpu;
  ActionQueue actions;
};

#endif /* _CPUTURNSTATE_H_ */
