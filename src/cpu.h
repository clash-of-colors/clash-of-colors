#ifndef _CPU_H_
#define _CPU_H_

#include "action.h"
#include "position.h"

#include <vector>

class Field;
class Scene;

class Cpu
{
public:
  struct Action
  {
    enum Type
    {
      Null,
      MoveUnit,
      RemoveUnit,
      CallReinforcements,
    };

    Action();
    Action(const Action& that);

    Type type;
    Position srcPos;
    int dstCol;
  };

  typedef std::vector<Action> Move;
  typedef std::vector<Move> MoveList;

  void makeMoveActions(ActionQueue& actions, Field* field, Field* otherField, Scene* scene);
  void analyzeTriples(Field* field, MoveList& moves);
};

#endif /* _CPU_H_ */
