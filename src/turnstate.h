#ifndef _TURNSTATE_H_
#define _TURNSTATE_H_

#include "state.h"

class Field;
class Scene;

class TurnState : public State
{
public:
  TurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm);
  virtual ~TurnState();
  
  void setup();

  Field* field;
  Field* otherField;
  Scene* scene;
  int moves;
};

#endif /* _TURNSTATE_H_ */
