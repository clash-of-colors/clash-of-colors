#ifndef _COMPONENTSCENENODE_H_
#define _COMPONENTSCENENODE_H_

#include "scenenode.h"

#include <vector>

class Component;
class SceneNodeRenderer;

class ComponentSceneNode : public SceneNode
{
public:
  ComponentSceneNode();
  ~ComponentSceneNode();

  void render() override;
  void update() override;

  void setRenderer(SceneNodeRenderer* renderer);
  SceneNodeRenderer* getRenderer() const;

  void addComponent(Component* component);

  template<class C>
  C* getComponent() const
  {
    for( auto c : components_ ) {

      C* result = dynamic_cast<C*>(c);
      if( result )
	return result;
    }

    return 0;
  }

private:
  SceneNodeRenderer* renderer_;
  std::vector<Component*> components_;
};

#endif /* _COMPONENTSCENENODE_H_ */
