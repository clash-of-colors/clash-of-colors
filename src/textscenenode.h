#ifndef _TEXTSCENENODE_H_
#define _TEXTSCENENODE_H_

#include "componentscenenode.h"

#include <string>

class TextSceneNode : public ComponentSceneNode
{
public:
  TextSceneNode(const Position& pos, const std::string& text);

  std::string text;
};

#endif /* _TEXTSCENENODE_H_ */
