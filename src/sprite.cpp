#include "sprite.h"

Sprite::Sprite(const int spriteID)
  : spriteID(spriteID)
  , offsetX(0.0f)
  , offsetY(0.0f)
{
}

Sprite::Sprite(const int spriteID, const float offsetX, const float offsetY)
  : spriteID(spriteID)
  , offsetX(offsetX)
  , offsetY(offsetY)
{
}
