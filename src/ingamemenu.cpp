#include "ingamemenu.h"
#include "menustate.h"
#include "titlestate.h"

#include <stm/statemanager.h>

InGameMenu::InGameMenu()
{
  addButton( Button( "EXIT TO TITLE", [](MenuState* state) {
	STM::StateManager* sm = state->getStateManager();
	sm->popAll();
	sm->pushState( new TitleState(sm), true );
      } ) );
}

