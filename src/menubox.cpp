#include "menubox.h"
#include "tilemap.h"
#include "tilemaprenderer.h"

#include <algorithm>

static TileMap* makeBoxMap(const int size, const int sprite, const int w, const int h)
{
  int numCols = w / size;
  if( numCols * size < w || numCols < 2 ) {

    numCols = std::max(2, numCols + 1);
  }

  int numRows = h / size;
  if( numRows * size < h || numRows < 2 ) {

    numRows = std::max(2, numRows + 1);
  }

  TileMap* map = new TileMap(numCols, numRows, size, size);
  
  int i = 0;
  map->set( i++, sprite + 0 );
  for( int col = 1; col < numCols - 1; ++col ) {
    map->set( i++, sprite + 1 );
  }
  map->set( i++, sprite + 2 );
  
  for( int row=1; row < numRows - 1; ++row ) {
    map->set( i++, sprite + 3 );
    for( int col = 1; col < numCols - 1; ++col ) {
      map->set( i++, sprite + 4 );
    }
    map->set( i++, sprite + 5 );
  }

  map->set( i++, sprite + 6 );
  for( int col = 1; col < numCols - 1; ++col ) {
    map->set( i++, sprite + 7 );
  }
  map->set( i++, sprite + 8 );

  return map;
}

MenuBox::MenuBox(const Position& pos, const int w, const int h, const int spriteID)
  : w_(w)
  , h_(h)
  , map_(0)
{
  this->pos = pos;

  map_ = makeBoxMap(8, spriteID, w, h);

  w_ = map_->getNumCols() * map_->getTileWidth();
  this->pos.x -= (w_ - w) / 2;
  h_ = map_->getNumRows() * map_->getTileHeight();
  this->pos.y -= (h_ - h) / 2;

  setRenderer( new TileMapRenderer(map_) );
}

MenuBox::~MenuBox()
{
  delete map_;
}
