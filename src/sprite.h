#ifndef _SPRITE_H_
#define _SPRITE_H_

class Sprite
{
public:
  Sprite(const int spriteID = -1);
  Sprite(const int spriteID, const float offsetX, const float offsetY);

  int spriteID;
  float offsetX;
  float offsetY;
};

#endif /* _SPRITE_H_ */
