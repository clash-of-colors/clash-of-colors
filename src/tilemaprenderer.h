#ifndef _TILEMAPRENDERER_H_
#define _TILEMAPRENDERER_H_

#include "scenenoderenderer.h"

class TileMap;

class TileMapRenderer : public SceneNodeRenderer
{
public:
  TileMapRenderer(TileMap* map);
  void render(SceneNode* node) override;

private:
  TileMap* map_;
};

#endif /* _TILEMAPRENDERER_H_ */
