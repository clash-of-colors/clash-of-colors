#include "alphaosc.h"
#include "aniscript.h"
#include "blinker.h"
#include "componentscenenode.h"
#include "scenenode.h"
#include "spriteanim.h"
#include "spriteanimrenderer.h"

#include <cassert>
#include <cmath>
#include <cstring> // memcpy
#include <iostream>

namespace ani
{
  Context::Context(const Script* script, SceneNode* node)
  : script_(script)
  , commandIterator_(script)
  , node_(node)
  , stackIndex_(0)
  , yielded_(false)
  {
    if( !commandIterator_.atEnd() ) {

      commandIterator_.getCommand().setup(this, node_);
    }
  }

  Context::~Context()
  {
  }
  
  void Context::push(const char* data, int size)
  {
    assert( stackIndex_ + size <= StackSize );
    memcpy(&stackData_[stackIndex_], data, size);
    stackIndex_ += size;
  }

  void Context::pop(char* data, int size)
  {
    peek(data, size);
    stackIndex_ -= size;
  }

  void Context::peek(char* data, int size, int offset) const
  {
    const int index = stackIndex_ - size - offset;
    assert( index >= 0);
    memcpy(data, &stackData_[index], size);
  }

  bool Context::isFinished() const
  {
    if( !commandIterator_.atEnd() ) {

      return false;
    }

    return areChildrenFinished();
  }

  bool Context::areChildrenFinished() const
  {
    for( auto child : children_ ) {

      if( !child->isFinished() ) {

	return false;
      }
    }

    return true;
  }

  void Context::update()
  {
    yielded_ = false;
    while( !yielded_ && !commandIterator_.atEnd() ) {
      
      const Command& cmd = commandIterator_.getCommand();
      cmd.update( this, node_ );
      if( cmd.isFinished(this) ) {
	
	cmd.cleanup(this, node_);
	commandIterator_.next();
	
	if( !commandIterator_.atEnd() ) {
	  
	  commandIterator_.getCommand().setup(this, node_);
	}
      }
    }
    
    for( auto child : children_ ) {

      child->update();
    }
  }

  void Context::createChild(const Script* script)
  {
    children_.push_back(new Context(script, node_));
  }

  void Context::yield()
  {
    yielded_ = true;
  }

  void Context::setNode(SceneNode* node)
  {
    node_ = node;
  }

  Command::Command(CommandImpl* impl)
    : impl_(impl)
  {
  }

  bool Command::isFinished(const Context* ctx) const
  {
    return impl_->isFinished(ctx);
  }

  void Command::setup(Context* ctx, SceneNode* node) const
  {
    impl_->setup(ctx, node);
  }

  void Command::cleanup(Context* ctx, SceneNode* node) const
  {
    impl_->cleanup(ctx, node);
  }

  void Command::update(Context* ctx, SceneNode* node) const
  {
    impl_->update(ctx, node);
  }

  namespace 
  {
    struct SetNode : public CommandImpl
    {
      SceneNode* node;
      
      SetNode(SceneNode* node)
      : node(node)
      {
      }

      bool isFinished(const Context* /*ctx*/) const override
      {
	return true;
      }

      void setup(Context* ctx, SceneNode* /*node*/) const override
      {
	ctx->setNode(node);
      }

      void cleanup(Context* /*ctx*/, SceneNode* /*node*/) const override
      {
      }

      void update(Context* /*ctx*/, SceneNode* /*node*/) const override
      {
      }
    };
  } // ns 

  Script::Script(SceneNode* node)
  {
    if( node ) 
      commands_.push_back(new SetNode(node));
  }

  Script::Script(const Command& cmd)
  {
    commands_.push_back(cmd);
  }

  Script::Script(CommandImpl* cmd)
  {
    commands_.push_back(Command(cmd));
  }

  Script::~Script()
  {
  }

  Script& Script::operator<<( const Script& rhs )
  {
    commands_.insert( commands_.end(), rhs.commands_.begin(), rhs.commands_.end() );
    return *this;
  }

  namespace
  {
    struct Fork : public CommandImpl
    {
      Fork(const Script& child)
	: child_(child)
      {
      }

      bool isFinished(const Context* /*ctx*/) const override
      {
	return true;
      }

      void setup(Context* /*ctx*/, SceneNode* /*node*/) const override
      {
      }

      void cleanup(Context* /*ctx*/, SceneNode* /*node*/) const override
      {
      }

      void update(Context* ctx, SceneNode* /*node*/) const override
      {
	ctx->createChild(&child_);
      }

      Script child_;
    };
  } // ns

  Script Script::operator&( const Script& rhs ) const
  {
    Script copy(*this);
    copy.commands_.push_back(new Fork(rhs));
    return copy;
  }

  Script& Script::operator&=( const Script& rhs )
  {
    commands_.push_back( new Fork(rhs) );
    return *this;
  }

  namespace 
  {
    struct MoveBy : public CommandImpl
    {
      float dx_;
      float dy_;
      int frames_;
      InterpolateFunctor interpolate_;

      MoveBy( const float dx, const float dy, const int frames, const InterpolateFunctor& interpolate )
	: dx_(dx)
	, dy_(dy)
	, frames_(frames)
	, interpolate_(interpolate)
      {
	assert(frames > 0);
      }

      bool isFinished(const Context* ctx) const override
      {
	return ctx->peek<int>() <= 0;
      }

      void setup(Context* ctx, SceneNode* /*node*/) const override
      {
	ctx->push(0.0f); // dx0
	ctx->push(0.0f); // dy0
	ctx->push(frames_); // n
      }

      void cleanup(Context* ctx, SceneNode* node) const override
      {
	ctx->pop<int>(); // n
	ctx->pop<float>(); // dy0
	ctx->pop<float>(); // dx0

	node->pos.x = roundf(node->pos.x);
	node->pos.y = roundf(node->pos.y);
      }

      void update(Context* ctx, SceneNode* node) const override
      {
	const int n = ctx->pop<int>() - 1;
	const float dy0 = ctx->pop<float>();
	const float dx0 = ctx->pop<float>();

	const float arg = 1.0f - static_cast<float>(n) / static_cast<float>(frames_);
	const float dx1 = interpolate_(0, dx_, arg);
	const float dy1 = interpolate_(0, dy_, arg);
	
	node->pos.x += dx1 - dx0;
	node->pos.y += dy1 - dy0;

	ctx->push(dx1);
	ctx->push(dy1);
	ctx->push(n);
	ctx->yield();
      }
    };
  } // ns 

  Script moveBy(const float dx, const float dy, const int frames, const InterpolateFunctor& interpolate)
  {
    return Command(new MoveBy(dx, dy, frames, interpolate));
  }

  namespace 
  {
    struct MoveTo : public CommandImpl
    {
      float x_;
      float y_;
      int frames_;
      InterpolateFunctor interpolate_;
      
      MoveTo(const float x, const float y, const int frames, const InterpolateFunctor& interpolate)
	: x_(x)
	, y_(y)
	, frames_(frames)
	, interpolate_(interpolate)
      {
      }

      bool isFinished(const Context* ctx) const override
      {
	return ctx->peek<int>() <= 0;
      }

      void setup(Context* ctx, SceneNode* node) const override
      {
	ctx->push(node->pos.x); // x0
	ctx->push(node->pos.y); // y0
	ctx->push(frames_); // n
      }

      void cleanup(Context* ctx, SceneNode* node) const override
      {
	ctx->pop<int>(); // n
	ctx->pop<float>(); // y0
	ctx->pop<float>(); // x0

	node->pos.x = roundf(node->pos.x);
	node->pos.y = roundf(node->pos.y);
      }

      void update(Context* ctx, SceneNode* node) const override
      {
	const float x0 = ctx->peek<float>(sizeof(float) * 2);
	const float y0 = ctx->peek<float>(sizeof(float) * 1);
	const int n = ctx->pop<int>() - 1;
	const float arg = 1.0f - static_cast<float>(n) / static_cast<float>(frames_);
      
	node->pos.x = interpolate_(x0, x_, arg);
	node->pos.y = interpolate_(y0, y_, arg);
	
	ctx->push(n);
	ctx->yield();
      }
      
    };
  } // ns 

  Script moveTo(const float x, const float y, const int frames, const InterpolateFunctor& interpolate)
  {
    return new MoveTo(x, y, frames, interpolate);
  }

  struct FloatSetter : public CommandImpl
  {
    float SceneNode::* ptr_;
    float value_;
    int frames_;
    InterpolateFunctor interpolate_;

    FloatSetter(float SceneNode::* ptr, const float value, const int frames, const InterpolateFunctor& interpolate)
      : ptr_(ptr)
      , value_(value)
      , frames_(frames)
      , interpolate_(interpolate)
    {
    }

    bool isFinished(const Context* ctx) const override
    {
      return ctx->peek<int>() <= 0;
    }

    void setup(Context* ctx, SceneNode* node) const override
    {
      ctx->push(node->*ptr_); // value0
      ctx->push(frames_); // n
    }

    void cleanup(Context* ctx, SceneNode* /*node*/) const override
    {
      ctx->pop<int>(); // n
      ctx->pop<float>(); // value0
    }

    void update(Context* ctx, SceneNode* node) const override
    {
	const int n = ctx->pop<int>() - 1;
	const float value0 = ctx->peek<float>();

	const float arg = 1.0f - static_cast<float>(n) / static_cast<float>(frames_);
	
	node->*ptr_ = interpolate_(value0, value_, arg);

	ctx->push(n);
	ctx->yield();
    }
  };

  Script setAlpha(const float value, const int frames, const InterpolateFunctor& interpolate)
  {
    return new FloatSetter(&SceneNode::alpha, value, frames, interpolate);
  }
  
  template<class ComponentClass>
  struct ComponentTrigger : public CommandImpl
  {
    typedef void (ComponentClass::*MethodPtr)(void);

    MethodPtr methodPtr_;
    SceneNode* node_;

    ComponentTrigger(MethodPtr methodPtr, SceneNode* node = 0)
    : methodPtr_(methodPtr)
    , node_(node)
    {
    }

    bool isFinished(const Context*) const override
    {
      return true;
    }

    void setup(Context*, SceneNode*) const override
    {
    }

    void cleanup(Context*, SceneNode*) const override
    {
    }

    void update(Context*, SceneNode* node) const override
    {
      ComponentSceneNode* csn = dynamic_cast<ComponentSceneNode*>( node_ ? node_ : node );
      if( csn ) {

	ComponentClass* component = csn->getComponent<ComponentClass>();
	if( component ) {

	  (component->*methodPtr_)();
	}
      }
    }
  };

  Script resetAlphaOsc(SceneNode* node)
  {
    return new ComponentTrigger<AlphaOsc>(&AlphaOsc::reset, node);
  }

  struct Kill : public CommandImpl
  {
    SceneNode* node_;

    Kill(SceneNode* node)
    : node_(node)
    {
    }

    bool isFinished(const Context*) const override
    {
      return true;
    }

    void setup(Context*, SceneNode*) const override {}
    void cleanup(Context*, SceneNode*) const override {}

    void update(Context*, SceneNode* node) const override
    {
      (node_ ? node_ : node)->active = false;
    }
  };
  
  Script kill(SceneNode* node)
  {
    return new Kill(node);
  }

  struct Join : public CommandImpl
  {
    bool isFinished(const Context* ctx) const override
    {
      return ctx->areChildrenFinished();
    }

    void setup(Context*, SceneNode*) const override {}
    void cleanup(Context*, SceneNode*) const override {}

    void update(Context* ctx, SceneNode*) const override 
    {
      if( !isFinished(ctx) )
	ctx->yield();
    }
  };

  Script join()
  {
    return new Join;
  }

  struct Wait : CommandImpl
  {
    const int frames_;

    Wait(const int frames)
    : frames_(frames)
    {
    }

    bool isFinished(const Context* ctx) const override
    {
      return ctx->peek<int>() <= 0;
    }
    
    void setup(Context* ctx, SceneNode*) const override
    {
      ctx->push(frames_);
    }

    void cleanup(Context* ctx, SceneNode*) const override
    {
      ctx->pop<int>();
    }

    void update(Context* ctx, SceneNode*) const override
    {
      const int n = ctx->pop<int>() - 1;
      ctx->push(n);
      ctx->yield();
    }
  };

  Script wait(const int frames)
  {
    return new Wait(frames);
  }

  template<typename T>
  struct InstantSetter : public CommandImpl
  {
    SceneNode* node_;
    T SceneNode::* ptr_;
    T value_;

    InstantSetter(SceneNode* node, T SceneNode::* ptr, const T value)
      : node_(node)
      , ptr_(ptr)
      , value_(value)
    {
    }

    bool isFinished(const Context*) const override
    {
      return true;
    }

    void setup(Context*, SceneNode*) const override 
    {
    }

    void cleanup(Context*, SceneNode*) const override 
    {
    }

    void update(Context*, SceneNode* node) const override 
    {
      (node_ ? node_ : node)->*ptr_ = value_;
    }
  };

  Script setVisible(SceneNode* node, bool visible)
  {
    return new InstantSetter<bool>(node, &SceneNode::visible, visible);
  }

  struct WaitForSpriteAnim : public CommandImpl
  {
    bool isFinished(const Context* ctx) const override
    {
      const spani::Runner* runner = ctx->peek<spani::Runner*>();
      return !runner || runner->isFinished();
    }

    void setup(Context* ctx, SceneNode* node) const override
    {
      ComponentSceneNode* csn = dynamic_cast<ComponentSceneNode*>(node);
      if( !csn ) {

	std::cerr << "ERROR: WaitForSpriteAnim: Not a ComponentSceneNode!" << std::endl;
	ctx->push<spani::Runner*>(0);
      }
      else {
	
	SpriteAnimRenderer* san = dynamic_cast<SpriteAnimRenderer*>(csn->getRenderer());
	if( !san ) {

	  std::cerr << "ERROR: WaitForSpriteAnim: Not a SpriteAnimRenderer!" << std::endl;
	  ctx->push<spani::Runner*>(0);
	}
	else {

	  ctx->push(&san->getRunner());
	}
      }
    }

    void cleanup(Context* ctx, SceneNode*) const override
    {
      ctx->pop<spani::Runner*>();
    }

    void update(Context* ctx, SceneNode*) const override
    {
      ctx->yield();
    }
  };

  Script waitForSpriteAnim()
  {
    return new WaitForSpriteAnim;
  }

  Script startBlink()
  {
    return new ComponentTrigger<Blinker>(&Blinker::start);
  }

  Script stopBlink()
  {
    return new ComponentTrigger<Blinker>(&Blinker::stop);
  }

  static float clampArg(const float arg)
  {
    if( arg < 0.0f ) {

      return 0.0f;
    }
    else if( arg > 1.0f ) {

      return 1.0f;
    }
    else {

      return arg;
    }
  }

  float interpolateLinear(const float a, const float b, const float arg)
  {
    return a + (b - a) * clampArg(arg);
  }

  float interpolateSmoothstep(const float a, const float b, const float argIn)
  {
    const float arg = clampArg(argIn);
    return a + (b - a) * arg * arg * (3.0f - arg * 2.0f);
  }

} // ns aniscript
