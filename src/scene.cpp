#include "globals.h"
#include "scene.h"
#include "scenenode.h"

#include <algorithm> // sort, remove_if

Scene::Scene()
{
  nodes.reserve(1000);
}

Scene::~Scene()
{
  for( auto node : nodes ) {

    delete node;
  }
}

void Scene::addNode( SceneNode* node )
{
  nodes.push_back(node);
}

void Scene::update()
{
  for( auto node : nodes ) {
    
    node->update();
  }

  auto end = std::remove_if( nodes.begin(), 
			     nodes.end(), 
			     [](const SceneNode* node)
			     {
			       return !node->active;
			     } );

  nodes.erase(end, nodes.end());
}

static bool cmpNodes(const SceneNode* n1, const SceneNode* n2)
{
  if( n1->layer < n2->layer ) {

    return true;
  }
  else if( n1->layer > n2->layer ) {

    return false;
  }
  else if( n1->pos.y < n2->pos.y ) {

    return true;
  }
  else if( n1->pos.y > n2->pos.y ) {

    return false;
  }
  else {

    return n1->pos.x < n2->pos.x;
  }
}

void Scene::render()
{
  std::sort( nodes.begin(), nodes.end(), cmpNodes );
  for( auto node : nodes ) {

    if( !node->visible || !node->active )
      continue;

    node->render();
  }
}
