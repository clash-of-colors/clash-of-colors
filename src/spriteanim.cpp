#include "spriteanim.h"
#include "spriterenderer.h"

#include <cassert>
#include <cmath>

namespace spani
{
  Step::~Step()
  {
  }

  void Step::setup(Runner* /*runner*/) const
  {
  }

  struct Frame : public Step
  {
    Frame(const int id, const float dx, const float dy)
      : id(id)
      , dx(dx)
      , dy(dy)
    {
    }

    void setup(Runner* runner) const override
    {
      runner->setCounter(1);
    }

    void execute(Runner* runner) const override
    {
      if( runner->getCounter() ) {

	runner->setCounter(0);
	runner->setSprite( Sprite(id, dx, dy) );
	runner->yield();
      }
    }

    int id;
    float dx;
    float dy;
  };
  
  StepPtr frame(const int id, const float dx, const float dy)
  {
    return StepPtr(new Frame(id, dx, dy));
  }

  struct Wait : public Step
  {
    Wait(const int n)
    : n(n)
    {
    }

    void setup(Runner* runner) const override
    {
      runner->setCounter(n);
    }

    void execute(Runner* runner) const override
    {
      if( runner->getCounter() > 0 ) {
	
	runner->setCounter( runner->getCounter() - 1 );
	runner->yield();
      }
    }

    int n;
  };

  StepPtr wait(const int n)
  {
    return StepPtr(new Wait(n));
  }

  struct Jump : public Step
  {
    Jump(const int i)
    : i(i - 1)
    {
    }
    
    void execute(Runner* runner) const override
    {
      runner->jump(i);
    }

    int i;
  };

  struct Halt : public Step
  {
    void execute(Runner* runner) const override
    {
      runner->yield();
    }
  };

  StepPtr halt()
  {
    return StepPtr(new Halt);
  }
  
  Anim::Anim(const int fps)
    : fps_(fps)
  {
  }
  
  Anim& Anim::operator<<( StepPtr step )
  {
    steps_.push_back(step);
    return *this;
  }

  Anim& Anim::operator<<( const Loop& loop )
  {
    const int i = (int) steps_.size();
    steps_.insert(steps_.end(), loop.steps_.begin(),  loop.steps_.end());
    steps_.push_back(StepPtr(new Jump(i)));
    return *this;
  }

  Loop& Loop::operator<<( StepPtr step )
  {
    steps_.push_back(step);
    return *this;
  }

  Runner::Runner(const int fps)
    : fps_(fps)
    , anim_(0)
  {
  }

  void Runner::setAnim(const Anim* anim)
  {
    assert(anim);
    assert(!anim->steps_.empty());

    anim_ = anim;
    index_ = -1;
    sprite_ = Sprite();
    frameTicks_ = 0;
    frameDelay_ = std::ceil(static_cast<float>( fps_ ) / static_cast<float>( anim->fps_ ));

    update();
  }
  
  bool Runner::isFinished() const
  {
    assert(anim_);
    return index_ >= (int) anim_->steps_.size();
  }

  void Runner::setSprite(const Sprite& sprite)
  {
    sprite_ = sprite;
  }

  const Sprite& Runner::getSprite() const
  {
    return sprite_;
  }

  void Runner::setCounter(const int counter)
  {
    counter_ = counter;
  }

  int Runner::getCounter() const
  {
    return counter_;
  }

  void Runner::jump(const int index)
  {
    index_ = index;
  }

  void Runner::yield()
  {
    yielded_ = true;
  }

  void Runner::halt()
  {
    yielded_ = true;
    index_ = (int) anim_->steps_.size();
  }

  void Runner::next()
  {
    index_++;
    if( !isFinished() ) {

      const StepPtr& step = anim_->steps_[index_];
      step->setup(this);
    }
  }

  void Runner::update()
  {
    assert(anim_);

    if( frameTicks_ > 0 ) {

      frameTicks_--;
      return;
    }
    else {

      frameTicks_ = frameDelay_;
    }

    if( index_ == -1 ) 
      next();

    yielded_ = false;
    while( !isFinished() && !yielded_ ) {

      const StepPtr& step = anim_->steps_[index_];
      step->execute(this);
      if( !yielded_ ) {

	next();
      }
    }
  }

} // ns spani
