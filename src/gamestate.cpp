#include "cputurnstate.h"
#include "gamestate.h"
#include "globals.h"
#include "healthscenenode.h"
#include "ingamemenu.h"
#include "menustate.h"
#include "playerturnstate.h"
#include "sprites.h"
#include "textscenenode.h"

#include <stm/statemanager.h>

#include <tmepp/video.h>

GameState::GameState(STM::StateManager* stm, const bool cpuMatch)
  : State(stm)
  , fieldCpu(0, FieldY0, -1)
  , fieldPlayer(0, FieldY1, +1)
  , cpuTurnState(0)
  , playerTurnState(0)
{
  setName("GameState");

  fieldCpu.scene = &scene;
  fieldPlayer.scene = &scene;

  cpuTurnState = new CpuTurnState( &fieldCpu, &fieldPlayer, &scene, stm );
  
  if( cpuMatch ) {

    playerTurnState = new CpuTurnState( &fieldPlayer, &fieldCpu, &scene, stm );

    scene.addNode(new TextSceneNode(Position(TextAreaX, FieldY0), "-CPU 2-"));
    scene.addNode(new TextSceneNode(Position(TextAreaX, FieldY1), "-CPU 1-"));
  }
  else {
    
    playerTurnState = new PlayerTurnState( &fieldPlayer, &fieldCpu, &scene, stm );

    scene.addNode(new TextSceneNode(Position(TextAreaX, FieldY0), "-CPU-"));
    scene.addNode(new TextSceneNode(Position(TextAreaX, FieldY1), "-PLAYER-"));
  }

  scene.addNode(new HealthSceneNode(&fieldCpu, FieldY0 + 10));
  scene.addNode(new HealthSceneNode(&fieldPlayer, FieldY1 + 10));
}

GameState::~GameState()
{
  delete playerTurnState;
  delete cpuTurnState;
}

void GameState::onTransition(STM::State* prevState)
{
  if( getTransitionState() == TransitionOut ) {

    setTransitionState(TransitionOutDone);
    return;
  }

  if( getStateManager()->isQueueEmptyAfterTransition() ) {

    if( prevState == playerTurnState ) {
      
      cpuTurnState->setup();
      getStateManager()->pushState(cpuTurnState);
    }
    else if( prevState == cpuTurnState ) {
      
      playerTurnState->setup();
      getStateManager()->pushState(playerTurnState);
    }
    else {
      
      playerTurnState->setup();
      getStateManager()->pushState(playerTurnState);
    }
  }
    
  setTransitionState(TransitionInDone);
}

void GameState::update()
{
  scene.update();
}

void GameState::render()
{
  Tme::Video::blitSprite(SPRITE_FIELD_BG, FieldX0, FieldY0);
  Tme::Video::blitSprite(SPRITE_FIELD_BG, FieldX0, FieldY1);

  scene.render();
}

bool GameState::onInput(VPAD* vpad)
{
  if( vpad->justPressed(VPAD::ButtonStart) ) {

    getStateManager()->pushState( new MenuState(getStateManager(), InGameMenu(), &scene), true );
  }

  return true;
}
