#ifndef _WAITACTION_H_
#define _WAITACTION_H_

#include "action.h"

struct WaitAction : public Action
{
  bool isDone() const override { return true; }
  void update() override {}
};

#endif /* _WAITACTION_H_ */
