#include "field.h"
#include "globals.h"
#include "movecursorstate.h"
#include "moveunitstate.h"
#include "playerturnstate.h"
#include "unit.h"

#include <stm/statemanager.h>

#include <stdlib.h> // rand

#include <cassert>
#include <iostream>

PlayerTurnState::PlayerTurnState(Field* field, Field* otherField, Scene* scene, STM::StateManager* stm)
  : TurnState(field, otherField, scene, stm)
  , moveCursorState(new MoveCursorState(actions, field, scene, stm))
{
  setName("PlayerTurnState");
}

PlayerTurnState::~PlayerTurnState()
{
  delete moveCursorState;
}

void PlayerTurnState::update()
{
}

void PlayerTurnState::render()
{
  g_font->print( TextAreaX, FieldY1 + 20, "MOVES: %d", moves );
}

void PlayerTurnState::onTransition(STM::State* prevState)
{
  if( getTransitionState() == TransitionOut ) {

    setTransitionState(TransitionOutDone);
    return;
  }

  if( getStateManager()->isQueueEmptyAfterTransition() ) {
  
    if( prevState == moveCursorState ) {

      if( moveCursorState->endTurn ) {

	getStateManager()->popState();
      }
      else {

	moves--;
	if( moves <= 0 ) {
	
	  getStateManager()->popState();
	}
	else {
	
	  moveCursorState->setup(Field::Cols / 2);
	  getStateManager()->pushState(moveCursorState);
	}
      }
    }
    else {

      field->onStartOfTurn();
      field->checkAttack(otherField, actions);

      moveCursorState->setup(Field::Cols / 2);
      getStateManager()->pushState(moveCursorState);
    }
  }
  
  setTransitionState(TransitionInDone);
}
