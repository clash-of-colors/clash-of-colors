#ifndef _UTIL_H_
#define _UTIL_H_

//! Copy a range to two outputs, using a predicate to determine
//! which output to use.
//!
//! Similar to std::partiton_stable.
//!
//! If pred(item) returns true, item is copied to outTrue, otherwise
//! to outFalse.
//!
//! Example:
//! \code
//! std::vector<int> input{ 1, -1, 0, 3, -2 };
//! std::vector<int> nonNegatives;
//! std::vector<int> negatives;
//!
//! partition_stable_copy( input.begin(), 
//!                        input.end(), 
//!                        std::back_inserter(nonNegatives), 
//!                        std::back_inserter(negatives),
//!                        [](const int i) {
//!                            return i >= 0;
//!                        } );
//! \endcode
template<class InputIterator, class OutputIterator, class Pred>
void partition_stable_copy( InputIterator begin, InputIterator end, 
			    OutputIterator outTrue, 
			    OutputIterator outFalse,
			    Pred pred)
{
  for( ; begin != end; ++begin ) {
    
    if( pred(*begin) ) {

      *outTrue = *begin;
      ++outTrue;
    }
    else {

      *outFalse = *begin;
      ++outFalse;
    }
  }
}

#endif /* _UTIL_H_ */
