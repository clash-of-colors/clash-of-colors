#ifndef _SCENENODERENDERER_H_
#define _SCENENODERENDERER_H_

class SceneNode;

class SceneNodeRenderer
{
public:
  virtual ~SceneNodeRenderer() {}
  virtual void render(SceneNode* sceneNode) = 0;
};

#endif /* _SCENENODERENDERER_H_ */
