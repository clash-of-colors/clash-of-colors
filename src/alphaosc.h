#ifndef _ALPHAOSC_H_
#define _ALPHAOSC_H_

#include "component.h"

class AlphaOsc : public Component
{
public:
  AlphaOsc(const float frequency, const float amplitude);
  void update(SceneNode* node) override;
  void reset();

private:
  float arg_;
  float dArg_;
  float amplitude_;
};

#endif /* _ALPHAOSC_H_ */
