#include "action.h"
#include "field.h"

void updateActionQueue(ActionQueue& actions, Field* field, Field* otherField)
{
  if( actions.empty() ) {
    if( otherField ) {
      otherField->checkActions(actions);
    }
    if( actions.empty() ) {
      field->checkActions(actions);
      if( actions.empty() ) {
	return;
      }
    }
  }
  
  actions.front()->update();
  if( actions.front()->isDone() ) {
    actions.pop();
    if( actions.empty() ) {
      if( otherField ) {
	otherField->checkActions(actions);
      }
      if( actions.empty() ) {
	field->checkActions(actions);
      }
    }
  }
}
