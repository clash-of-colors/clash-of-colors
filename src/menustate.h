#ifndef _MENUSTATE_H_
#define _MENUSTATE_H_

#include "action.h"
#include "menu.h"
#include "state.h"

class MenuBox;

class Scene;

class MenuState : public State
{
public:
  MenuState(STM::StateManager* stateManager, const Menu& menu, Scene* scene);
  void update() override;
  void onTransition(STM::State*) override;
  bool onInput(VPAD* vpad) override;

private:
  void updateCursorPos();

  Menu menu_;
  Scene* scene_;
  ActionQueue actions_;
  MenuBox* cursorBox_;
  int cursorIndex_;
  VPADRepeater vpadRepeater_;
};

#endif /* _MENUSTATE_H_ */
