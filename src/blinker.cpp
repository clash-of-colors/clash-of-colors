#include "blinker.h"
#include "fps.h"
#include "scenenode.h"

Blinker::Blinker(const float frequency)
  : interval_(fps::seconds(0.5f / frequency))
  , ticks_(0)
{
}

void Blinker::start()
{
  ticks_ = 0;
}

void Blinker::stop()
{
  ticks_ = -1;
}

void Blinker::update(SceneNode* node)
{
  if( ticks_ == -1 )
    return;

  ticks_++;
  if( ticks_ >= interval_ ) {

    ticks_ = 0;
    node->visible = !node->visible;
  }
}
