#include "globals.h"
#include "scenenoderenderer.h"
#include "textscenenode.h"

#include <cmath>

class TextRenderer : public SceneNodeRenderer
{
public:
  TextRenderer(const std::string& text);
  void render(SceneNode* node) override;

private:
  std::string text_;
};

TextRenderer::TextRenderer(const std::string& text)
  : text_(text)
{
}

void TextRenderer::render(SceneNode* node)
{
  g_font->printBlended( roundf(node->pos.x), roundf(node->pos.y), 
			static_cast<unsigned char>(roundf(node->alpha * 255.0f)),
			text_.c_str() );
}

TextSceneNode::TextSceneNode(const Position& pos, const std::string& text)
{
  this->pos = pos;
  setRenderer(new TextRenderer(text));
}
