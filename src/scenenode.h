#ifndef _SCENENODE_H_
#define _SCENENODE_H_

#include "position.h"

class SceneNode
{
public:
  SceneNode();
  virtual ~SceneNode() {}

  virtual void update() {}
  virtual void render() {}

  int layer;
  Position pos;
  bool active;
  bool visible;
  float alpha;
};

#endif /* _SCENENODE_H_ */
