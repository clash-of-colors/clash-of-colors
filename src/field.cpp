#include "aniscript.h"
#include "componentscenenode.h"
#include "field.h"
#include "fps.h"
#include "globals.h"
#include "layers.h"
#include "puff.h"
#include "runscriptaction.h"
#include "scene.h"
#include "spriterenderer.h"
#include "sprites.h"
#include "unit.h"
#include "unittable.h"
#include "util.h"
#include "waitaction.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>

#include <string.h> // memset

static bool cmpUnits(const Unit* u1, const Unit* u2)
{
  if( u1->fieldPos.y < u2->fieldPos.y ) {
    
    return true;
  }
  else if( u1->fieldPos.y > u2->fieldPos.y ) {
    
    return false;
  }
  else {
    
    return u1->fieldPos.x < u2->fieldPos.x;
  }
}

Field::Field(Scene* scene, const int y0, const float sy)
  : scene(scene)
  , health(100)
  , y0_(y0)
  , sy_(sy)
{
  units_.reserve(100);
}

Field::~Field()
{
}

bool Field::canDropUnit( const Unit* unit, int col ) const
{
  if( col + unit->width > Cols ) {
    return false;
  }

  return getDropRow(unit, col) + unit->height <= Rows;
}

int Field::getDropRow( const Unit* unitToDrop, const int col ) const
{
  const int a0 = col;
  const int a1 = col + unitToDrop->width - 1;

  int row = 0;
  for( auto const unit : units_ ) {

    if( unit == unitToDrop )
      continue;

    const int b0 = unit->fieldPos.x;
    const int b1 = unit->fieldPos.x + unit->width - 1;

    if( a0 <= b1 && a1 >= b0 ) {

      // Cols overlap
      row = std::max(row, static_cast<int>(unit->fieldPos.y) + unit->height);
    }
  }

  return row;
}

void Field::dropUnit( Unit* unit, const int col )
{
  const int row = getDropRow(unit, col);
  unit->fieldPos.x = col;
  unit->fieldPos.y = row;
  units_.push_back(unit);
}

Unit* Field::getUnitAt( const Position& pos ) const
{
  for( auto unit : units_ ) {
    
    if( unit->fieldPos.x <= pos.x && unit->fieldPos.x + unit->width > pos.x &&
	unit->fieldPos.y <= pos.y && unit->fieldPos.y + unit->height > pos.y ) {

      return unit;
    }
  }

  return 0;
}

Unit* Field::takeUnitAt( const Position& pos )
{
  Unit* unit = getUnitAt(pos);
  return unit ? takeUnit(unit) : 0;
}

void Field::getUnitsInCols(const int col, const int numCols, std::vector<Unit*>& result) const
{
  for( auto unit : units_ ) {
    
    if( unit->fieldPos.x < col + numCols && 
	unit->fieldPos.x + unit->width > col ) {
      
      result.push_back(unit);
    }
  }
}

static void checkVerticalMerge( const UnitTable& table,
				const int col0,
				const int width,
				const int row0,
				const int type,
				const int height,
				const int color,
				std::vector<Unit*>& unitsToMerge )
{
  unitsToMerge.clear();

  for( int row = row0; row < UnitTable::Rows; row += height ) {

    bool complete = true;
    for( int col = col0; col < col0 + width; ++col ) {

      Unit* unit = table.get(col, row);
      if( !unit 
	  || unit->type != type 
	  || unit->color != color 
	  || (unit->width > 1 && unit->fieldPos.x != col0) ) {
	
	complete = false;
	break;
      }
    }

    if(!complete)
      break;

    for( int col = col0; col < col0 + width; ++col ) {

      Unit* unit = table.get(col, row);
      unitsToMerge.push_back(unit);
    }
  }
}

static SceneNode* makeWhiteUnit(const Unit* unit)
{
  ComponentSceneNode* node = new ComponentSceneNode;
  node->pos = unit->pos;
  node->layer = layers::above(unit->layer);
  node->alpha = 0.0f;

  SpriteRenderer* spriteRenderer = new SpriteRenderer;
  switch( unit->type ) {
    
  case Unit::Single:
    spriteRenderer->setSprite(SPRITE_WHITE_SINGLE);
    break;
  case Unit::TripleCharging:
    spriteRenderer->setSprite(SPRITE_WHITE_TRIPLE);
    break;
  case Unit::Quad:
  case Unit::QuadCharging:
    spriteRenderer->setSprite(SPRITE_WHITE_QUAD);
    break;
  case Unit::Wall:
    spriteRenderer->setSprite(SPRITE_WHITE_WALL);
    break;
  default:
    assert(false);
  }
  node->setRenderer(spriteRenderer);
  return node;
}

void Field::mergeUnits( Unit* bottomUnit, 
			const std::vector<Unit*>& unitsToMerge,
			Unit* mergedUnit,
			ActionQueue& actions )
{
  takeUnit(bottomUnit);

  ani::Script script;
  SceneNode* whiteUnit(makeWhiteUnit(bottomUnit));
  scene->addNode(whiteUnit);
  script &= ( ani::Script(whiteUnit) 
	      << ani::setAlpha(1.0f, fps::seconds(0.5f), ani::interpolateSmoothstep) 
	      << ani::kill(bottomUnit)
	      << ani::setAlpha(0.0f, fps::seconds(0.5f), ani::interpolateSmoothstep) 
	      << ani::kill() );

  for( auto unitToTake : unitsToMerge ) {

    takeUnit(unitToTake);

    SceneNode* whiteUnit(makeWhiteUnit(unitToTake));
    scene->addNode(whiteUnit);

    script &= ( ani::Script(whiteUnit) 
		<< ani::setAlpha(1.0f, fps::seconds(0.5f), ani::interpolateSmoothstep) 
		<< ani::kill(unitToTake)
		<< ani::setAlpha(0.0f, fps::seconds(0.5f), ani::interpolateSmoothstep) 
		<< ani::kill() );
  }

  mergedUnit->fieldPos = bottomUnit->fieldPos;
  mergedUnit->alpha = 0.0;
  setUnitPos(mergedUnit);
  units_.push_back(mergedUnit);
  scene->addNode(mergedUnit);
	    
  script << ani::join() << ani::Script(mergedUnit) << ani::setAlpha(1.0, fps::seconds(0.5f), ani::interpolateSmoothstep);
	    
  std::cerr << "Action: Vertical merge" << std::endl;
  actions.push(ActionPtr(new RunScriptAction(script)));
}

void Field::makeWalls(const std::vector<Unit*>& wallUnits, ActionQueue& actions)
{
  ani::Script script;
  for( auto oldUnit : wallUnits ) {

    takeUnit(oldUnit);

    Unit* wallUnit = new Unit(Unit::Wall, Unit::Yellow);
    wallUnit->fieldPos = oldUnit->fieldPos;
    wallUnit->pos = oldUnit->pos;
    wallUnit->visible = false;
    scene->addNode(wallUnit);
    units_.push_back(wallUnit);

    SceneNode* whiteUnit = makeWhiteUnit(oldUnit);
    scene->addNode(whiteUnit);

    script << ani::wait(fps::seconds(0.1));
    script &= ( ani::Script(whiteUnit)
		<< ani::setAlpha(1.0f, fps::seconds(0.5), ani::interpolateSmoothstep)
		<< ani::kill(oldUnit)
		<< ani::setVisible(wallUnit, true)
		<< ani::setAlpha(0.0f, fps::seconds(0.5), ani::interpolateSmoothstep)
		<< ani::kill() );
  }

  std::cerr << "Action: Wall merge" << std::endl;
  actions.push(ActionPtr(new RunScriptAction(script)));
}

void Field::checkActions(ActionQueue& actions)
{
  // Units are assumed to remain sorted throughout all
  // individual checks below - it is required that checks
  // terminate after they touched "units_"

  std::sort( units_.begin(), 
	     units_.end(),
	     cmpUnits );
  
  UnitTable table(units_);
  
  // Check for drops...
  {
    int numDrops = 0;
    ani::Script script;

    for(auto unit : units_) {
      
      const int col = unit->fieldPos.x;
      const int row0 = unit->fieldPos.y;
      if( row0 == 0 )
	continue; // Already at bottom...
      
      bool blocked = false;
      int row1 = row0;
      
      while( !blocked && row1 > 0 ) {
	
	const int nextRow = row1 - 1;
	for( int i = 0; i < unit->width; ++i ) {
	  
	  if( table.get(col + i, nextRow) ) {
	    blocked = true;
	    break;
	  }
	}
	if( !blocked ) {
	  row1 = nextRow;
	}
      }

      if( row1 < row0 ) {

	table.resetUnit(unit);
	unit->fieldPos.y = row1;
	table.setUnit(unit);

	if( numDrops ) {

	  // Small delay between drops
	  script << ani::wait(fps::seconds(0.1));
	}

	const Position pos1 = getUnitPos(unit);

	script &= ( ani::Script(unit)
		    << ani::moveTo( pos1.x,
				    pos1.y,
				    fps::distanceVelocity(pos1.y - unit->pos.y, MoveVelocity),
				    ani::interpolateSmoothstep ) );

	numDrops++;
      }
    }

    if( numDrops ) {

      std::cerr << "Action: Reg. drop" << std::endl;
      actions.push(ActionPtr(new RunScriptAction(script)));
      return;
    }
  }

  // Check for "heavy" drops
  {
    std::vector<Unit*> unitsBelow;
    unitsBelow.reserve(units_.size());
    std::vector<Unit*> unitsIgnored;
    unitsIgnored.reserve(units_.size());

    for( auto heavyUnit : units_ ) {

      if( !heavyUnit->getWeight() || heavyUnit->fieldPos.y == 0 ) {

	continue;
      }

      const int col0 = heavyUnit->fieldPos.x;
      const int col1 = heavyUnit->fieldPos.x + heavyUnit->width - 1;

      unitsBelow.clear();
      unitsIgnored.clear();

      int row = 0;
      for( auto unit : units_ ) {

	if( unit->fieldPos.y >= heavyUnit->fieldPos.y ) {
	  
	  // This check also handles unit == heavyUnit
	  unitsIgnored.push_back(unit);
	}
	else {

	  const int otherCol0 = unit->fieldPos.x;
	  const int otherCol1 = unit->fieldPos.x + unit->width - 1;
	  if( col0 <= otherCol1 && col1 >= otherCol0 ) {
	    
	    const int tmpRow = unit->fieldPos.y + unit->height;
	    if( unit->getWeight() >= heavyUnit->getWeight() ) {
	      
	      row = std::max(row, tmpRow);
	      unitsIgnored.push_back(unit);
	    }
	    else {
	      
	      unitsBelow.push_back(unit);
	    }
	  }
	  else {

	    unitsIgnored.push_back(unit);
	  }
	}
      }
    
      if( row >= heavyUnit->fieldPos.y )
	continue; // Cannot move...
    
      units_.swap(unitsIgnored);

      heavyUnit->fieldPos.y = row;
      setUnitPos(heavyUnit); // TODO: Animate...

      for( auto unit : unitsBelow ) {

	const int tmpRow = unit->fieldPos.y + unit->height;
	if( tmpRow <= row ) {

	  units_.push_back(unit);
	}
	else {

	  dropUnit(unit, unit->fieldPos.x);
	  setUnitPos(unit); // TODO: Animate
	}
      }

      // TODO
      std::cerr << "Action: Heavy. drop" << std::endl;
      actions.push(ActionPtr(new WaitAction));
      return;
    }
  }

  // Check vertical merges
  {
    std::vector<Unit*> unitsToMerge;
    unitsToMerge.reserve(20);

    for( auto bottomUnit : units_ ) {

      if( bottomUnit->fieldPos.y + bottomUnit->height >= Rows )
	continue;

      switch(bottomUnit->type) {

      case Unit::Single:
	{
	  checkVerticalMerge( table, 
			      bottomUnit->fieldPos.x, 
			      bottomUnit->width,
			      bottomUnit->fieldPos.y + bottomUnit->height,
			      bottomUnit->type,
			      1,
			      bottomUnit->color,
			      unitsToMerge );

	  if( unitsToMerge.size() >= 2 ) {

	    // TODO: Handle bonus for extra units

	    Unit* mergedUnit = new Unit(Unit::TripleCharging, bottomUnit->color);
	    mergedUnit->turnsToDeploy = 3;
	    mergeUnits( bottomUnit, unitsToMerge, mergedUnit, actions );
	    return;
	  }
	}
	break;
      
      case Unit::Quad:
	{
	  checkVerticalMerge( table, 
			      bottomUnit->fieldPos.x, 
			      bottomUnit->width,
			      bottomUnit->fieldPos.y + bottomUnit->height,
			      Unit::Single,
			      1,
			      bottomUnit->color,
			      unitsToMerge );

	  if( unitsToMerge.size() >= 4 ) {

	    Unit* mergedUnit = new Unit(Unit::QuadCharging, bottomUnit->color);
	    mergedUnit->turnsToDeploy = 3;
	    mergeUnits( bottomUnit, unitsToMerge, mergedUnit, actions );
	    return;
	  }

	  checkVerticalMerge( table, 
			      bottomUnit->fieldPos.x, 
			      bottomUnit->width,
			      bottomUnit->fieldPos.y + bottomUnit->height,
			      Unit::Quad,
			      2,
			      bottomUnit->color,
			      unitsToMerge );

	  if( unitsToMerge.size() >= 1 ) {

	    Unit* mergedUnit = new Unit(Unit::QuadCharging, bottomUnit->color);
	    mergedUnit->turnsToDeploy = 3;
	    mergeUnits( bottomUnit, unitsToMerge, mergedUnit, actions );
	    return;
	  }
	}
	break;
      
      case Unit::TripleCharging:
	{
	  checkVerticalMerge( table,
			      bottomUnit->fieldPos.x,
			      bottomUnit->width,
			      bottomUnit->fieldPos.y + bottomUnit->height,
			      bottomUnit->type,
			      3,
			      bottomUnit->color,
			      unitsToMerge );
	  
	  if( unitsToMerge.size() >= 1 ) {

	    Unit* mergedUnit = new Unit(Unit::TripleCharging, bottomUnit->color);
	    mergedUnit->turnsToDeploy = bottomUnit->turnsToDeploy;
	    for( auto unitToMerge : unitsToMerge ) {

	      if( unitToMerge->turnsToDeploy != -1 ) {

		mergedUnit->turnsToDeploy = std::min(mergedUnit->turnsToDeploy, unitToMerge->turnsToDeploy);
	      }
	    }
	    mergeUnits( bottomUnit, unitsToMerge, mergedUnit, actions );
	    return;
	  }
	}
	break;

      case Unit::QuadCharging:
	{
	  checkVerticalMerge( units_,
			      bottomUnit->fieldPos.x,
			      bottomUnit->width,
			      bottomUnit->fieldPos.y + bottomUnit->height,
			      bottomUnit->type,
			      2,
			      bottomUnit->color,
			      unitsToMerge );
	  
	  if( unitsToMerge.size() >= 1 ) {
	    
	    Unit* mergedUnit = new Unit(Unit::QuadCharging, bottomUnit->color);
	    for( auto unitToMerge : unitsToMerge ) {

	      if( unitToMerge->turnsToDeploy != -1 ) {

		mergedUnit->turnsToDeploy = std::min(mergedUnit->turnsToDeploy, unitToMerge->turnsToDeploy);
	      }
	    }
	    mergeUnits( bottomUnit, unitsToMerge, mergedUnit, actions );
	    return;
	  }
	}
	break;

      case Unit::Wall:
	// TODO
	break;

      default:
	assert(false);
      }
    }
  }

  // Horizontal wall merges
  {
    std::vector<Unit*> wallUnits;
    wallUnits.reserve(Cols);

    for( int row = 0; row < Rows; ++row ) {

      wallUnits.clear();

      int color = -1;
      for( int col = 0; col < Cols; ++col ) {

	Unit* unit = table.get(col, row);
	if( !unit || unit->type != Unit::Single || (color != -1 && color != unit->color) ) {

	  if( wallUnits.size() >= 3 ) {

	    makeWalls(wallUnits, actions);
	    return;
	  }

	  color = -1;
	  wallUnits.clear();
	}
	else {
	  
	  color = unit->color;
	  wallUnits.push_back(unit);
	}
      }
      
      if( wallUnits.size() >= 3 ) {
	
	makeWalls(wallUnits, actions);
	return;
      }
    }
  }

  // Units pushed off field...
  {
    std::vector<Unit*> unitsToRemove;
    unitsToRemove.reserve(units_.size());
    std::vector<Unit*> unitsToKeep;
    unitsToKeep.reserve(units_.size());

    partition_stable_copy( units_.begin(), units_.end(), 
			   std::back_inserter(unitsToRemove),
			   std::back_inserter(unitsToKeep),
			   [](const Unit* unit) {
			     return unit->fieldPos.y + unit->height > Rows;
			   } );

    units_.swap(unitsToKeep);

    if( !unitsToRemove.empty() ) {


      ani::Script script;
      bool first = true;
      for( auto unit : unitsToRemove ) {

	if( first ) {

	  first = false;
	}
	else {

	  script << ani::wait(fps::seconds(0.1));
	}

	script << ani::Script(unit) 
	       << ani::setAlpha(0.0, fps::seconds(0.5), ani::interpolateSmoothstep)
	       << ani::kill();
      }

      actions.push(ActionPtr(new RunScriptAction(script)));
    }
  }
}

Unit* Field::takeUnit( Unit* unit )
{
  auto it = std::find(units_.begin(), units_.end(), unit);
  if( it == units_.end() ) {

    return 0;
  }
  else {

    *it = units_.back();
    units_.pop_back();
    return unit;
  }
}

struct UnitCollisionAction : public Action
{
  Unit* unitA_;
  Unit* unitB_;
  bool waiting_;

  UnitCollisionAction(Unit* unitA, Unit* unitB)
    : unitA_(unitA)
    , unitB_(unitB)
    , waiting_(false)
  {
  }

  bool isDone() const override
  {
    return waiting_
      && unitA_->visibleHealth == unitA_->health
      && unitB_->visibleHealth == unitB_->health;
  }
 
  void update() override
  {
    if( !waiting_ ) {

      waiting_ = true;

      const int dHealth = std::min(unitA_->health, unitB_->health);
      unitA_->health -= dHealth;
      unitB_->health -= dHealth;
    }
  }
};

static ani::Script makeDeathAnim(Unit* unit, Scene* scene)
{
  SceneNode* white = makeWhiteUnit(unit);
  scene->addNode(white);
	
  SceneNode* puff = makePuff(unit->getCenterPos());
  puff->layer = layers::above(unit->layer);
  puff->visible = false;
  scene->addNode(puff);

  ani::Script script;
  script << ani::Script(white)
	 << ani::setAlpha(1.0f, fps::seconds(0.5), ani::interpolateSmoothstep)
	 << ani::kill(unit)
         << ani::kill()
	 << ani::Script(puff)
         << ani::setVisible(puff, true)
         << ani::waitForSpriteAnim()
         << ani::kill();

  return script;
}

struct DamageAction : public Action
{
  Field* field;
  int damage;

  DamageAction(Field* field, const int damage)
    : field(field)
    , damage(damage)
  {
  }

  bool isDone() const override 
  {
    return true;
  }

  void update() override
  {
    field->health -= damage;
  }
};

void Field::checkAttack(Field* otherField, ActionQueue& actions)
{
  std::sort(units_.begin(), units_.end(), cmpUnits);

  std::vector<Unit*> otherUnits;
  otherUnits.reserve(20);
  
  for( auto unit : units_ ) {

    if( unit->turnsToDeploy != 0 )
      continue;

    unit->layer = layers::above(unit->layer); // Render on top of walls

    takeUnit(unit);

    otherUnits.clear();
    otherField->getUnitsInCols(unit->fieldPos.x, unit->width, otherUnits);
    std::sort(otherUnits.begin(), otherUnits.end(), cmpUnits);

    int health = unit->health;

    Position pos0 = unit->pos;

    for( auto otherUnit : otherUnits ) {

      {
	Position pos1(pos0);
	if( sy_ == -1 ) {

	  pos1.y = otherUnit->pos.y - unit->height * CellH /*+ 2 * CellMarginY*/;
	}
	else {
	  
	  pos1.y = otherUnit->pos.y + otherUnit->height * CellH /*- 2 * CellMarginY*/;
	}
	
	if( std::abs(pos1.y - pos0.y) > 0.1 ) {

	  ani::Script script(unit);
	  script << ani::moveTo( pos1.x, pos1.y,
				 fps::distanceVelocity(pos1.y - pos0.y, MoveVelocity),
				 ani::interpolateSmoothstep );
	  
	  actions.push(ActionPtr(new RunScriptAction(script)));
	}

	pos0 = pos1;
      }

      actions.push(ActionPtr(new UnitCollisionAction(unit, otherUnit)));
      
      ani::Script script;

      health -= std::min(health, otherUnit->health);
      if( health <= 0 ) {

	script &= makeDeathAnim(unit, scene);
      }
      if( health >= 0 ) {

	otherField->takeUnit(otherUnit);
	script << makeDeathAnim(otherUnit, scene);
      }
      
      script << ani::join();
      actions.push(ActionPtr(new RunScriptAction(script)));

      if( health <= 0 ) {
	break;
      }
    }

    if( health > 0 ) {

      Position pos1(pos0);
      if( sy_ == -1 ) {

	pos1.y = ScreenH;
      }
      else {

	pos1.y = 0 - unit->height * CellH /*+ 2 * CellMarginY*/;
      }

      // TODO: Death anim
      // TODO: Oponent damage
      ani::Script script(unit);
      script << ani::moveTo(pos1.x, pos1.y,
			    fps::distanceVelocity(pos1.y - pos0.y, MoveVelocity),
			    ani::interpolateSmoothstep)
	     << ani::kill();
      actions.push(ActionPtr(new RunScriptAction(script)));
      actions.push(ActionPtr(new DamageAction(otherField, health)));
    }
  }

  otherField->checkActions(actions);
}

void Field::onStartOfTurn()
{
  for( auto unit : units_ ) {

    if( unit->turnsToDeploy > 0 ) {
      
      unit->turnsToDeploy--;
    }
  }
}

const std::vector<Unit*>& Field::getUnits() const
{
  return units_;
}

void Field::setUnitPos(Unit* unit) const
{
  unit->pos = getUnitPos(unit);
}

Position Field::getUnitPos(const Unit* unit) const
{
  return getUnitPos(unit, unit->fieldPos.x, unit->fieldPos.y);
}

Position Field::getUnitPos(const Unit* unit, const int col, const int row) const
{
  const float x = FieldX0 /*+ Field::CellMarginX*/ + col * Field::CellW;

  if( sy_ == 1 ) {
    
    const float y = y0_ /*+ Field::CellMarginY*/ + row * Field::CellH;
    return Position(x, y);
  }
  else {

    const float y = y0_ /*+ Field::CellMarginY*/ + FieldHeight - (row + unit->height) * Field::CellH;
    return Position(x, y);
  }
}

Position Field::getUnitFloatingPos(const Unit* unit, const int col) const
{
  const float x = FieldX0 /*+ Field::CellMarginX*/ + col * Field::CellW;
  
  if( sy_ == 1 ) {

    const float y = FieldY1 + FieldHeight + 4;
    return Position(x, y);
  }
  else {

    const float y = FieldY0 - 4 - unit->height * CellH /*+ 2 * CellMarginY*/;
    return Position(x, y);
  }
}

Position Field::getUnitSpawnPos(const Unit* unit, const int col) const
{
  const float x = FieldX0 /*+ Field::CellMarginX*/ + col * Field::CellW;

  if( sy_ == 1 ) {

    const float y = ScreenH;
    return Position(x, y);
  }
  else {

    const float y = 0 - unit->height * CellH /*+ 2 * CellMarginY*/;
    return Position(x, y);
  }
}
