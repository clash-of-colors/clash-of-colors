#ifndef _RUNSCRIPTACTION_H_
#define _RUNSCRIPTACTION_H_

#include "action.h"
#include "aniscript.h"

class RunScriptAction : public Action
{
public:
  RunScriptAction(const ani::Script& script);

  bool isDone() const override;
  void update() override;

private:
  ani::Script script_;
  ani::Context context_;
};

#endif /* _RUNSCRIPTACTION_H_ */
