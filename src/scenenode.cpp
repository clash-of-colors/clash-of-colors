#include "layers.h"
#include "scenenode.h"

SceneNode::SceneNode()
  : layer(layers::base())
  , active(true)
  , visible(true)
  , alpha(1.0f)
{
}
