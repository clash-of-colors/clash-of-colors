#include "field.h"
#include "fps.h"
#include "globals.h"
#include "spriterenderer.h"
#include "sprites.h"
#include "unit.h"

#include <tmepp/video.h>

#include <cassert>

namespace 
{
  const int A_FPS = 10;

  const spani::Anim A_SINGLE_YELLOW = spani::Anim(A_FPS)
    << spani::frame(SPRITE_SINGLE_YELLOW);
  const spani::Anim A_SINGLE_BLUE = spani::Anim(A_FPS)
    << spani::frame(SPRITE_SINGLE_BLUE);
  const spani::Anim A_SINGLE_RED = spani::Anim(A_FPS)
    << spani::frame(SPRITE_SINGLE_RED);

  const spani::Anim A_QUAD_YELLOW = spani::Anim(A_FPS)
    << spani::frame(SPRITE_QUAD_YELLOW);
  const spani::Anim A_QUAD_BLUE = spani::Anim(A_FPS)
    << spani::frame(SPRITE_QUAD_BLUE);
  const spani::Anim A_QUAD_RED = spani::Anim(A_FPS)
    << spani::frame(SPRITE_QUAD_RED);

  const spani::Anim A_WALL = spani::Anim(A_FPS)
    << spani::frame(SPRITE_WALL);

  const spani::Anim A_QUAD_CHARGING_YELLOW = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_QUAD_YELLOW_CHARGE_0)
	 << spani::frame(SPRITE_QUAD_YELLOW_CHARGE_1)
	 << spani::frame(SPRITE_QUAD_YELLOW_CHARGE_2)
	 << spani::frame(SPRITE_QUAD_YELLOW_CHARGE_3) );
  const spani::Anim A_QUAD_CHARGING_BLUE = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_QUAD_BLUE_CHARGE_0)
	 << spani::frame(SPRITE_QUAD_BLUE_CHARGE_1)
	 << spani::frame(SPRITE_QUAD_BLUE_CHARGE_2)
	 << spani::frame(SPRITE_QUAD_BLUE_CHARGE_3) );
  const spani::Anim A_QUAD_CHARGING_RED = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_QUAD_RED_CHARGE_0)
	 << spani::frame(SPRITE_QUAD_RED_CHARGE_1)
	 << spani::frame(SPRITE_QUAD_RED_CHARGE_2)
	 << spani::frame(SPRITE_QUAD_RED_CHARGE_3) );

  const spani::Anim A_TRIPLE_CHARGING_YELLOW = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_TRIPLE_YELLOW_CHARGE_0)
	 << spani::frame(SPRITE_TRIPLE_YELLOW_CHARGE_1)
	 << spani::frame(SPRITE_TRIPLE_YELLOW_CHARGE_2)
	 << spani::frame(SPRITE_TRIPLE_YELLOW_CHARGE_3) );
  const spani::Anim A_TRIPLE_CHARGING_BLUE = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_TRIPLE_BLUE_CHARGE_0)
	 << spani::frame(SPRITE_TRIPLE_BLUE_CHARGE_1)
	 << spani::frame(SPRITE_TRIPLE_BLUE_CHARGE_2)
	 << spani::frame(SPRITE_TRIPLE_BLUE_CHARGE_3) );
  const spani::Anim A_TRIPLE_CHARGING_RED = spani::Anim(A_FPS)
    << ( spani::Loop()
	 << spani::frame(SPRITE_TRIPLE_RED_CHARGE_0)
	 << spani::frame(SPRITE_TRIPLE_RED_CHARGE_1)
	 << spani::frame(SPRITE_TRIPLE_RED_CHARGE_2)
	 << spani::frame(SPRITE_TRIPLE_RED_CHARGE_3) );

  const spani::Anim* const A_TABLE[Unit::NumTypes][Unit::NumColors] = {

    { &A_SINGLE_YELLOW, &A_SINGLE_RED, &A_SINGLE_BLUE }, 
    { &A_QUAD_YELLOW, &A_QUAD_RED, &A_QUAD_BLUE }, 
    { &A_TRIPLE_CHARGING_YELLOW, &A_TRIPLE_CHARGING_RED, &A_TRIPLE_CHARGING_BLUE }, 
    { &A_QUAD_CHARGING_YELLOW, &A_QUAD_CHARGING_RED, &A_QUAD_CHARGING_BLUE }, 
    { &A_WALL, &A_WALL, &A_WALL }
  };

} // ns 

void Unit::setType(const Type type)
{
  switch(type) {

  case Single:
  case Wall:
    width = 1;
    height = 1;
    break;

  case Quad:
  case QuadCharging:
    width = 2;
    height = 2;
    break;
    
  case TripleCharging:
    width = 1;
    height = 3;
    break;

  default:
    assert(false);
  }

  animRunner_.setAnim(A_TABLE[type][color]);
}

//bool Unit::isHeavy() const
//{
//  return type == TripleCharging || type == QuadCharging || type == Wall;
//}

int Unit::getWeight() const
{
  switch( type ) {

  case TripleCharging:
  case QuadCharging:
    return 1;
  case Wall:
    return 2;
  default:
    return 0;
  }
}

Position Unit::getCenterPos() const
{
  return Position( pos.x + (width * Field::CellW /*- 2 * Field::CellMarginX*/) / 2,
		   pos.y + (height * Field::CellH /*- 2 * Field::CellMarginY*/) / 2 );
}

Unit::Unit(Type type, Color color)
  : type(type)
  , color(color)
  , turnsToDeploy(-1)
  , animRunner_(FPS)
  , spriteRenderer_(new SpriteRenderer)
{
  setType(type);

  switch(type) {

  case Single:
    visibleHealth = health = 5;
    break;
  case Wall:
    visibleHealth = health = 15;
    break;
  case Quad:
    visibleHealth = health = 15;
    break;
  case QuadCharging:
    visibleHealth = health = 30;
    break;
  case TripleCharging:
    visibleHealth = health = 15;
    break;
  default:
    assert(false);
  }
}

Unit::~Unit()
{
}

void Unit::update()
{
  animRunner_.update();

  if( visibleHealth != health ) {

    // TODO: Count up/down
    visibleHealth = health;
  }
}

void Unit::render()
{
  spriteRenderer_->setSprite(animRunner_.getSprite());
  spriteRenderer_->render(this);

  const int x2 = pos.x + (width * Field::CellW) - (2 * Field::CellMarginX) - 2;

  getSmallFont()->printBlended( roundf(x2 - (3 * 4) + 1),
				roundf(pos.y + Field::CellMarginY + 2),
				static_cast<unsigned char>(roundf(alpha * 255.0f)),
				"%3d", visibleHealth );

  if( turnsToDeploy != -1 && alpha > 0.999 ) {

    g_font->print( roundf(x2 - 8), 
		   roundf(pos.y + Field::CellMarginY + 8), 
		   "%d", turnsToDeploy );
  }

}

Tme::SpriteFont* Unit::getSmallFont() const
{
  if( type == Wall ) {

    return g_fontSmallNumWhite;
  }
  else {

    return g_fontSmallNumBlack;
  }
}
