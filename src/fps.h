#ifndef _FPS_H_
#define _FPS_H_

enum {

  FPS = 60
};

namespace fps
{
  //! Seconds -> frames
  //! Always returns a value >= 1
  int seconds(const float s);

  //! Distance and velocity -> frames
  //! Always returns a value >= 1
  int distanceVelocity(const float distance, const float velocity);
} // ns fps

#endif /* _FPS_H_ */
