#ifndef _TITLESTATE_H_
#define _TITLESTATE_H_

#include "action.h"
#include "scene.h"
#include "state.h"

class SpriteNode;
class TextSceneNode;

class TitleState : public State
{
 public:
  TitleState(STM::StateManager* stateManager);

  void update() override;
  void render() override;
  void onTransition(STM::State*) override;

  bool onInput(VPAD* vpad) override;
  
 private:
  Scene scene_;
  ActionQueue actions_;

  SpriteNode* logo_;
  TextSceneNode* text_;
};

#endif /* _TITLESTATE_H_ */
