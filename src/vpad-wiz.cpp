#include "vpad.h"

#include <SDL.h>

enum WizButtons
{
  WizUpButton = 0x00,
  WizUpLeftButton,
  WizLeftButton,
  WizDownLeftButton,
  WizDownButton,
  WizDownRightButton,
  WizRightButton,
  WizUpRightButton,
  WizMenuButton,
  WizSelectButton,
  WizLeftShoulderButton,
  WizRightShoulderButton,
  WizAButton,
  WizBButton,
  WizXButton,
  WizYButton,
  WizVolPlusButton,
  WizVolMinusButton
};

struct WizVPADAdapter : public VPADAdapter
{
  enum {
    CounterUp,
    CounterDown,
    CounterLeft,
    CounterRight,
  };

  VPAD* vpad;
  int counters[4];
  
  WizVPADAdapter(VPAD* vpad)
  : vpad(vpad)
  {
    memset(counters, 0, sizeof(counters));
  }

  void onButton(const int button, const bool pressed)
  {
    switch(button) {
    case WizMenuButton:
      vpad->setPressed(VPAD::ButtonStart, pressed);
      break;
    case WizSelectButton:
      vpad->setPressed(VPAD::ButtonSelect, pressed);
      break;
    case WizLeftShoulderButton:
      vpad->setPressed(VPAD::ButtonTriggerLeft, pressed);
      break;
    case WizRightShoulderButton:
      vpad->setPressed(VPAD::ButtonTriggerRight, pressed);
      break;
    case WizAButton:
      vpad->setPressed(VPAD::ButtonY, pressed);
      break;
    case WizBButton:
      vpad->setPressed(VPAD::ButtonA, pressed);
      break;
    case WizXButton:
      vpad->setPressed(VPAD::ButtonB, pressed);
      break;
    case WizYButton:
      vpad->setPressed(VPAD::ButtonX, pressed);
      break;
    case WizVolPlusButton:
      vpad->setPressed(VPAD::ButtonVolumeUp, pressed);
      break;
    case WizVolMinusButton:
      vpad->setPressed(VPAD::ButtonVolumeDown, pressed);
      break;
    }
  }

  void onSDLEvent(SDL_Event* event) override
  {
    if( event->type == SDL_JOYBUTTONDOWN ) {
      switch( event->jbutton.button ) {
      case WizUpButton:
	counters[CounterUp]++;
	break;
      case WizUpRightButton:
	counters[CounterUp]++;
	counters[CounterRight]++;
	break;
      case WizUpLeftButton:
	counters[CounterUp]++;
	counters[CounterLeft]++;
	break;
      case WizDownButton:
	counters[CounterDown]++;
	break;
      case WizDownRightButton:
	counters[CounterDown]++;
	counters[CounterRight]++;
	break;
      case WizDownLeftButton:
	counters[CounterDown]++;
	counters[CounterLeft]++;
	break;
      case WizLeftButton:
	counters[CounterLeft]++;
	break;
      case WizRightButton:
	counters[CounterRight]++;
	break;
      default:
	onButton(event->jbutton.button, true);
	break;
      }
    }
    else if( event->type == SDL_JOYBUTTONUP ) {
      switch( event->jbutton.button ) {
      case WizUpButton:
	counters[CounterUp]--;
	break;
      case WizUpRightButton:
	counters[CounterUp]--;
	counters[CounterRight]--;
	break;
      case WizUpLeftButton:
	counters[CounterUp]--;
	counters[CounterLeft]--;
	break;
      case WizDownButton:
	counters[CounterDown]--;
	break;
      case WizDownRightButton:
	counters[CounterDown]--;
	counters[CounterRight]--;
	break;
      case WizDownLeftButton:
	counters[CounterDown]--;
	counters[CounterLeft]--;
	break;
      case WizLeftButton:
	counters[CounterLeft]--;
	break;
      case WizRightButton:
	counters[CounterRight]--;
	break;
      default:
	onButton(event->jbutton.button, false);
	break;
      }
    }

    vpad->setPressed(VPAD::ButtonUp, counters[CounterUp] > 0);
    vpad->setPressed(VPAD::ButtonDown, counters[CounterDown] > 0);
    vpad->setPressed(VPAD::ButtonLeft, counters[CounterLeft] > 0);
    vpad->setPressed(VPAD::ButtonRight, counters[CounterRight] > 0);
  }
};

VPADAdapter* sysCreateVPADAdapter(VPAD* vpad)
{
  return new WizVPADAdapter(vpad);
}
