#!/bin/sh

set -e

SRC_DIR=$(dirname $(readlink -f "$0"))/..
# TODO: Set this externally
BUILD_DIR=${SRC_DIR}/build/wiz

. ${SRC_DIR}/version

COC_DIR=coc-${VERSION}
COC_ZIP=coc-${VERSION}.zip
COC_INI=coc-${VERSION}.ini

STRIP=arm-wiz-linux-gnu-strip
CXX=arm-wiz-linux-gnu-g++

install_bin()
{
    cp "${BUILD_DIR}/bin/$1" "${COC_DIR}/bin/${1}.bin"
    "$STRIP" -s "${COC_DIR}/bin/${1}.bin"
}

install_lib()
{
    cp "${BUILD_DIR}/lib/$1" "${COC_DIR}/lib/$1"
    "$STRIP" -s "${COC_DIR}/lib/$1"
}

install_toolchainlib()
{
    LIBFILE=$("$CXX" "-print-file-name=$1")
    cp -L "$LIBFILE" "${COC_DIR}/lib/$1"
    "$STRIP" -s "${COC_DIR}/lib/$1"
}

install_data()
{
    cp "${SRC_DIR}/data/$1" "${COC_DIR}/data/$1"
}

mkdir -p "${SRC_DIR}/wiz/stage"
cd "${SRC_DIR}/wiz/stage"

rm -rf "$COC_DIR" "$COC_ZIP" "$COC_INI"

mkdir -p ${COC_DIR}/bin
mkdir -p ${COC_DIR}/lib
mkdir -p ${COC_DIR}/data

install_bin coc

cp ${SRC_DIR}/wiz/coc.gpe ${COC_DIR}/bin/

install_lib libstm.so
install_lib libtinyxml.so
install_lib libtmepp.so
install_lib libpng.so.3

install_toolchainlib libgcc_s.so.1
install_toolchainlib libstdc++.so.6

install_data sprites.ssx
install_data sprites.png
install_data icon26.png

cat > "$COC_INI" <<EOF
[info]
name="Clash of Colors $VERSION"
icon="/${COC_DIR}/data/icon26.png"
path="/${COC_DIR}/bin/coc.gpe"
EOF

zip -9 -r "$COC_ZIP" "$COC_DIR" "$COC_INI"
